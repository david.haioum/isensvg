# IsenSVG project architecture description

## Summary

1. Goals
2. Requirements
3. Main features
4. Secondary features
5. Libraries used
6. HMI description
7. User case
8. class UML
9. Installation
10. Main class (entry point)

## 1. Goals

**What is isvg format ?**

IsenSVG language (Isen Scalable Vector Graphics) 
describes a set of vector-based graphics based on XML.

Coordinates, dimensions and object structures are settled in
a XML file. A specific system has been created to specify colors and font families.
Isen svg manage quick geometric shapes like Rectangles, Circles and so on.

more info at : [w3.org](http://www.w3.org/TR/2001/REC-SVG20010904)

The goal is to create a viewer of this specific files with :
- A code editor to write the isvg code.
- a drawing zone to display the result.
- a Menu bar to load and save files.
- A button to render the file in the drawing zone.

## 2. Requirements

- Decompose the project in many JAVA classes
- Do unit tests
- Use standard colors
- Highlight XML code in the editor
- Contextual menu in editor for xml auto-completion 
- Menus to save and load files

## 3. Main features

- Xml/isvg code editor
- Load and save files
- Xml auto-completion
- Drawing zone to display the result
- Button to render the file
- draw :
    - Rectangles
    - Circles
    - Ellipses
    - Squares
    - Lines
    - Poly lines
    - Polygons
    - Isen_magic (school logo implementation)
    - Text
- Error handling with Exceptions and Log files (Logger)


## 4. Secondary features

Some ideas here to implements :
- [+] means implemented
- [-] not implemented

- new svg structures possible : based on [w3school](https://w3schools.com/graphics/svg_intro.asp)
    - [-] Paths (Bezier curves)
    - [-] text sub-group
    - [-] hyperlinks
    - [-] blur effect
    - [-] drop shadows
    - [-] linear gradient
    - [-] radial gradient
    - [-] filters

- HMI features :
    - [+] dark mode
    - [+] custom look and field
    - [+] instant drawing
    - [+] keyboard shortcut to create instant template in isvg file
    - [+] template buttons (images-based buttons ) to add template in isvg file
    - [+] instant templates
    - [+] color picker
    - [+] line numbers in code editor (like in Intellij IDEA)
    - [+] slider to move up and down in code editor
    - [-] open multiple files in tabs
    
## 5. Libraries used

- [xml editor](https://github.com/kdekooter/xml-text-editor/tree/master/src/main/java/net/boplicity/xmleditor)
- [text line numbers](http://www.camick.com/java/source/TextLineNumber.java)

## 6. HMI description

here is the tree structure of the user interface in JAVA :

- JPanel (Main window | BorderLayout)
    - JMenuBar (Navigation bar | North)
    - JToolBar (Toolbar | South | FlowLayout)
        - JButtons ("Refresh")
    - JSplitPane (Split panel | Center)
        - JTabPane (Code editor | left)
        - JPanel (Drawing zone | right)
            - Graphics
    
## 7. User case

1. **Navigation bar** :
    - **click on "FILE"** :
        - **"NEW FILE"** : create a new empty isvg file in the code editor
        - **"SAVE FILE"** : open a dialog window to save the file on the disk
        the user can choose where the file will be saved and it name.
        - **"OPEN FILE"** : open a dialog window to choose the "isvg" on the disk
        display the file in the code editor and render directly the file.
        - **"QUIT"** : quit the program.

2. **TOOLBAR** :
    - **click on "REFRESH" button"** : render the isvg file in the drawing zone
        1. read the xml file
        2. parse the file
        3. create a list of graphical objects to display
        4. draw all graphical objects in the drawing zone

3. **XML CODE EDITOR** :
    - **TAB** : keyboard shortcut that open a contextual menu to help the user.    

## 8. UML class diagram

   see **[uml.pdf](uml.pdf)**

## 9. Installation

- Build from sources :

    **see [INSTALL.md](INSTALL.txt) file**

- Launch from JAR :

```bash
    cd target/
    java -jar IsenSVG_HAIOUM-1.0-RELEASE.jar
```

## 10. Main class (entry point)

- name : **IsenSvgMain**
- source : **src/main/java/org.david/IsenSvgMain.java**