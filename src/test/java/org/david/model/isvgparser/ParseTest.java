/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.model.isvgparser;

import org.david.geo.polyshapes.PolyGon;
import org.david.geo.polyshapes.PolyLine;
import org.david.geo.shapes.Point;
import org.david.geo.style.CustomColor;
import org.david.geo.style.Style;
import org.david.geo.transform.Transform;
import org.junit.*;

import org.xml.sax.ext.Attributes2Impl;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * test class for Parse utility class.
 */
public class ParseTest {
    @Test
    public void parseTransform() {
        //full case
        Transform fullTransform = new Transform(
                new Point(200, 200),
                40
        );
        //put some spaces to also test this case
        String fullInput = "  rotate(40)   translate(200 200)  ";
        assertEquals(
                fullTransform.getTheta(),
                Parse.parseTransform(fullInput).getTheta(),
                1
        );
        assertEquals(
                fullTransform.getTranslate().getX(),
                Parse.parseTransform(fullInput).getTranslate().getX()
        );
        assertEquals(
                fullTransform.getTranslate().getY(),
                Parse.parseTransform(fullInput).getTranslate().getY()
        );

        //translate only case
        Transform translateTransform = new Transform(
                new Point(200, 200)
        );
        String translateInput = "translate(200 200)";
        assertEquals(
                translateTransform.getTheta(),
                Parse.parseTransform(translateInput).getTheta(),
                1
        );
        assertEquals(
                translateTransform.getTranslate().getX(),
                Parse.parseTransform(translateInput).getTranslate().getX()
        );
        assertEquals(
                translateTransform.getTranslate().getY(),
                Parse.parseTransform(translateInput).getTranslate().getY()
        );

        //rotate only case
        Transform rotateTransform = new Transform(-40);
        String rotateInput = "rotate(-40)";
        assertEquals(
                rotateTransform.getTheta(),
                Parse.parseTransform(rotateInput).getTheta(),
                1
        );
        assertEquals(
                rotateTransform.getTranslate().getX(),
                Parse.parseTransform(rotateInput).getTranslate().getX()
        );
        assertEquals(
                rotateTransform.getTranslate().getY(),
                Parse.parseTransform(rotateInput).getTranslate().getY()
        );
    }

    @Test
    public void parsePolyGon() {
        PolyGon p = new PolyGon(
                new Style(CustomColor.BLUE, CustomColor.gray, 23), 3
        );
        p.addCoordinate(300, 200);
        p.addCoordinate(100, 34);
        p.addCoordinate(
                Math.round(23.3f),
                Math.round(45.4f)
        );
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "points",
                "points",
                "",
                "300,200  100,34  23.3,45.4"

        );
        a.addAttribute(
                "",
                "stroke",
                "stroke",
                "",
                "blue"

        );
        a.addAttribute(
                "",
                "fill",
                "fill",
                "",
                "gray"

        );
        a.addAttribute(
                "",
                "stroke-width",
                "stroke-width",
                "",
                "23"

        );
        assertEquals(
                CustomColor.BLUE,
                Parse.parsePolyGon(a).getStyle().getStroke()
        );
        assertEquals(
                CustomColor.gray,
                Parse.parsePolyGon(a).getStyle().getFill()
        );
        assertEquals(
                23,
                Parse.parsePolyGon(a).getStyle().getStrokeWidth(),
                1
        );
        assertEquals(
                p.getNumberOfPoints(),
                Parse.parsePolyGon(a).getNumberOfPoints()
        );
        if (p.getNumberOfPoints() == Parse.parsePolyGon(a).getNumberOfPoints()) {
            //browse the two arrays and check point coordinates
            for (int i = 0; i < p.getNumberOfPoints() ; i++) {
                assertEquals(
                        p.getX()[i],
                        Parse.parsePolyGon(a).getX()[i]
                );
                assertEquals(
                        p.getY()[i],
                        Parse.parsePolyGon(a).getY()[i]
                );
            }
        } else {
            fail("different size");
        }
    }

    @Test
    public void getAttributeValueOf() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "rect",
                "rect",
                "",
                "234"

        );
        assertEquals("not found", Parse.getAttributeValueOf("z", a));
        assertEquals("234", Parse.getAttributeValueOf("rect", a));
    }

    @Test
    public void parsePolyPoint() {
        String points = "300,200  100,34 \n \n23.3,45.4 ";
        //new array of point
        ArrayList<Point> tableOfPoints = new ArrayList<>();
        tableOfPoints.add(new Point(300, 200));
        tableOfPoints.add(new Point(100, 34));
        tableOfPoints.add(new Point(
                Math.round(23.3f),
                Math.round(45.4f))
        );
        //check table sizes
        assertEquals(tableOfPoints.size(), Parse.parsePolyPoint(points).size());
        if (tableOfPoints.size() == Parse.parsePolyPoint(points).size()) {
            //browse the two arrays and check point coordinates
            for (int i = 0; i < tableOfPoints.size() ; i++) {
                assertEquals(
                        tableOfPoints.get(i).getX(),
                        Parse.parsePolyPoint(points).get(i).getX()
                );
                assertEquals(
                        tableOfPoints.get(i).getY(),
                        Parse.parsePolyPoint(points).get(i).getY()
                );
            }
        } else {
            fail("different size");
        }
    }

    @Test
    public void parsePolyLine() {
        PolyLine p = new PolyLine(CustomColor.GREEN, 23, 3);
        p.addCoordinate(300, 200);
        p.addCoordinate(100, 34);
        p.addCoordinate(
                Math.round(23.3f),
                Math.round(45.4f)
        );
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "points",
                "points",
                "",
                "300,200  100,34  23.3,45.4"

        );
        a.addAttribute(
                "",
                "stroke",
                "stroke",
                "",
                "lime"

        );
        a.addAttribute(
                "",
                "stroke-width",
                "stroke-width",
                "",
                "23"

        );
        assertEquals(CustomColor.GREEN, Parse.parsePolyLine(a).getStyle().getStroke());
        assertEquals(23, Parse.parsePolyLine(a).getStyle().getStrokeWidth(), 1);
        assertEquals(p.getNumberOfPoints(), Parse.parsePolyLine(a).getNumberOfPoints());
        if (p.getNumberOfPoints() == Parse.parsePolyLine(a).getNumberOfPoints()) {
            //browse the two arrays and check point coordinates
            for (int i = 0; i < p.getNumberOfPoints() ; i++) {
                assertEquals(
                        p.getX()[i],
                        Parse.parsePolyLine(a).getX()[i]
                );
                assertEquals(
                        p.getY()[i],
                        Parse.parsePolyLine(a).getY()[i]
                );
            }
        } else {
            fail("different size");
        }
        Attributes2Impl a2 = new Attributes2Impl();
        a2.addAttribute(
                "",
                "points",
                "points",
                "",
                "not found"

        );
        assertNull(Parse.parsePolyLine(a2));

    }

    @Test
    public void parseLine() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "stroke-width",
                "stroke-width",
                "",
                "23"

        );
        a.addAttribute(
                "",
                "stroke",
                "stroke",
                "",
                "lime"

        );
        a.addAttribute(
                "",
                "x1",
                "x1",
                "",
                "87"

        );
        a.addAttribute(
                "",
                "y1",
                "y1",
                "",
                "237"

        );
        a.addAttribute(
                "",
                "x2",
                "x2",
                "",
                "134"

        );
        a.addAttribute(
                "",
                "y2",
                "y2",
                "",
                "45"

        );
        assertNull(Parse.parseLine(a).getStyle().getFill());
        assertEquals(CustomColor.green, Parse.parseLine(a).getStyle().getStroke());
        assertEquals(87, Parse.parseLine(a).getP1().getX());
        assertEquals(237, Parse.parseLine(a).getP1().getY());
        assertEquals(134, Parse.parseLine(a).getP2().getX());
        assertEquals(45, Parse.parseLine(a).getP2().getY());
        assertEquals(23f, Parse.parseLine(a).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void parseIsenMagic() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "x",
                "x",
                "",
                "230"

        );
        a.addAttribute(
                "",
                "y",
                "y",
                "",
                "456"

        );
        assertEquals(230, Parse.parseIsenMagic(a).getOrigin().getX());
        assertEquals(456, Parse.parseIsenMagic(a).getOrigin().getY());
        assertEquals("/isen.jpg", Parse.parseIsenMagic(a).getPath());
    }

    @Test
    public void parseEllipse() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "cx",
                "cx",
                "",
                "230"

        );
        a.addAttribute(
                "",
                "cy",
                "cy",
                "",
                "456"

        );
        a.addAttribute(
                "",
                "cy",
                "cy",
                "",
                "456"

        );
        a.addAttribute(
                "",
                "stroke",
                "stroke",
                "",
                "blue"

        );
        a.addAttribute(
                "",
                "stroke-width",
                "stroke-width",
                "",
                "5"

        );
        a.addAttribute(
                "",
                "fill",
                "fill",
                "",
                "none"

        );
        a.addAttribute(
                "",
                "rx",
                "rx",
                "",
                "12"

        );
        a.addAttribute(
                "",
                "ry",
                "ry",
                "",
                "56"

        );
        assertEquals(230, Parse.parseEllipse(a).getOrigin().getX());
        assertEquals(456, Parse.parseEllipse(a).getOrigin().getY());
        assertEquals(12, Parse.parseEllipse(a).getBigAxis());
        assertEquals(56, Parse.parseEllipse(a).getSmallAxis());
        assertTrue(Parse.parseEllipse(a).getStyle().getFill().isNone());
        assertEquals(CustomColor.BLUE, Parse.parseEllipse(a).getStyle().getStroke());
        assertEquals(5, Parse.parseEllipse(a).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void parseCircle() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "cx",
                "cx",
                "",
                "230"

        );
        a.addAttribute(
                "",
                "cy",
                "cy",
                "",
                "456"

        );
        a.addAttribute(
                "",
                "cy",
                "cy",
                "",
                "456"

        );
        a.addAttribute(
                "",
                "stroke",
                "stroke",
                "",
                "blue"

        );
        a.addAttribute(
                "",
                "stroke-width",
                "stroke-width",
                "",
                "5"

        );
        a.addAttribute(
                "",
                "fill",
                "fill",
                "",
                "none"

        );
        a.addAttribute(
                "",
                "r",
                "r",
                "",
                "6"

        );
        //expected : originCoordinate - radius
        assertEquals(224, Parse.parseCircle(a).getOrigin().getX());
        assertEquals(450, Parse.parseCircle(a).getOrigin().getY());
        assertEquals(6, Parse.parseCircle(a).getRadius());
        assertTrue(Parse.parseCircle(a).getStyle().getFill().isNone());
        assertEquals(CustomColor.BLUE, Parse.parseCircle(a).getStyle().getStroke());
        assertEquals(5, Parse.parseCircle(a).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void parseRectangle() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "x",
                "x",
                "",
                "230"

        );
        a.addAttribute(
                "",
                "y",
                "y",
                "",
                "456"

        );
        a.addAttribute(
                "",
                "width",
                "width",
                "",
                "456"

        );
        a.addAttribute(
                "",
                "height",
                "height",
                "",
                "503"

        );
        a.addAttribute(
                "",
                "stroke",
                "stroke",
                "",
                "blue"

        );
        a.addAttribute(
                "",
                "stroke-width",
                "stroke-width",
                "",
                "5"

        );
        a.addAttribute(
                "",
                "fill",
                "fill",
                "",
                "none"

        );
        a.addAttribute(
                "",
                "r",
                "r",
                "",
                "6"

        );
        a.addAttribute(
                "",
                "rx",
                "rx",
                "",
                "123"

        );
        a.addAttribute(
                "",
                "ry",
                "ry",
                "",
                "278"

        );
        //expected : originCoordinate - radius
        assertEquals(230, Parse.parseRectangle(a).getOrigin().getX());
        assertEquals(456, Parse.parseRectangle(a).getOrigin().getY());
        assertEquals(456, Parse.parseRectangle(a).getWidth());
        assertEquals(503, Parse.parseRectangle(a).getHeight());
        assertTrue(Parse.parseRectangle(a).getStyle().getFill().isNone());
        assertEquals(CustomColor.BLUE, Parse.parseRectangle(a).getStyle().getStroke());
        assertEquals(5, Parse.parseRectangle(a).getStyle().getStrokeWidth(), 1);
        assertEquals(123, Parse.parseRectangle(a).getRx());
        assertEquals(278, Parse.parseRectangle(a).getRy());

        Attributes2Impl a2 = new Attributes2Impl();
        assertEquals(0, Parse.parseRectangle(a2).getRx());
        assertEquals(0, Parse.parseRectangle(a2).getRy());

    }

    @Test
    public void parsePoint() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "x",
                "x",
                "",
                "230"

        );
        a.addAttribute(
                "",
                "y",
                "y",
                "",
                "456"

        );
        assertEquals(230, Parse.parsePoint("x", "y", a).getX());
        assertEquals(456, Parse.parsePoint("x", "y", a).getY());
    }

    @Test
    public void parseInt() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "x",
                "x",
                "",
                "230.143536"

        );
        a.addAttribute(
                "",
                "y",
                "y",
                "",
                "230.943536"

        );
        assertEquals(230, Parse.parseInt("x", a));
        assertEquals(231, Parse.parseInt("y", a));
    }

    @Test
    public void parseDouble() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "x",
                "x",
                "",
                "none"

        );
        a.addAttribute(
                "",
                "y",
                "y",
                "",
                "2.34767"

        );
        assertEquals(0.0, Parse.parseDouble("x", a), 1);
        assertEquals(2.34767, Parse.parseDouble("y", a), 1);
        assertEquals(0.0, Parse.parseDouble("z", a), 1);
    }

    @Test
    public void parseFloat() {
        Attributes2Impl a = new Attributes2Impl();
        a.addAttribute(
                "",
                "x",
                "x",
                "",
                "none"

        );
        a.addAttribute(
                "",
                "y",
                "y",
                "",
                "2.34767"

        );
        assertEquals(0.0f, Parse.parseFloat("x", a), 1);
        assertEquals(2.34767f, Parse.parseFloat("y", a), 1);
        assertEquals(0.0f, Parse.parseFloat("z", a), 1);
    }

    @Test
    public void parseColor() {
        assertEquals(CustomColor.decode("#ff00ff"), Parse.parseColor("#ff00ff"));
        assertNull(Parse.parseColor("not found"));
        assertTrue(Parse.parseColor("none").isNone());
        assertEquals(CustomColor.BLUE, Parse.parseColor("blue"));
    }

    @Test
    public void getStandardColor() {
        assertEquals(
                CustomColor.decode("#800000"),
                Parse.getStandardColor("maroon")
        );
        assertEquals(
                CustomColor.red,
                Parse.getStandardColor("red")
        );
        assertEquals(
                CustomColor.orange,
                Parse.getStandardColor("orange")
        );
        assertEquals(
                CustomColor.yellow,
                Parse.getStandardColor("yellow")
        );
        assertEquals(
                CustomColor.decode("#808000"),
                Parse.getStandardColor("olive")
        );
        assertEquals(
                CustomColor.decode("#800080"),
                Parse.getStandardColor("purple")
        );
        assertEquals(
                CustomColor.MAGENTA,
                Parse.getStandardColor("fuchsia")
        );
        assertEquals(
                CustomColor.white,
                Parse.getStandardColor("white")
        );
        assertEquals(
                CustomColor.decode("#00FF00"),
                Parse.getStandardColor("lime")
        );
        assertEquals(
                CustomColor.green,
                Parse.getStandardColor("green")
        );
        assertEquals(
                CustomColor.decode("#000080"),
                Parse.getStandardColor("navy")
        );
        assertEquals(
                CustomColor.BLUE,
                Parse.getStandardColor("blue")
        );
        assertEquals(
                CustomColor.BLACK,
                Parse.getStandardColor("black")
        );
        assertEquals(
                CustomColor.decode("#c0c0c0"),
                Parse.getStandardColor("silver")
        );
        assertEquals(
                CustomColor.gray,
                Parse.getStandardColor("gray")
        );
        assertTrue(Parse.getStandardColor("none").isNone());
    }
}