/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.model.isvgparser;

import org.david.geo.primitive.Primitive;
import org.david.geo.style.CustomColor;
import org.junit.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ISvgParserTest {
    private static ArrayList<Primitive> parserOutput;

    @Before
    public void setup() {
        String input = "<svg width=\"12cm\" height=\"4cm\" viewBox=\"0 0 1200 400\" xmlns=\"http://www.w3.org/2000/svg\">\n"
                + "  <g fill=\"green\" stroke=\"red\" stroke-width=\"20\">\n"
                + "   \t<rect x=\"0\" y=\"0\" width=\"400\" height=\"200\" rx=\"50\"  />\n"
                + "   \t<text x=\"10\" y=\"23\" >TEst</text>\n"
                + "   \t<line x1=\"3\" y1=\"1\" x2=\"5\" y2=\"100\"  />\n"
                + "   \t<circle cx=\"200\" cy=\"100\" r=\"45\" fill=\"lime\" stroke=\"blue\" stroke-width=\"30\"/>\n"
                + "   \t<rect x=\"2\" y=\"1\" height=\"50\" width=\"50\" />\n"
                + "   \t<polygon fill=\"none\" stroke=\"black\" stroke-width=\"4\" points=\"50,200 300,300, 400,400\" />\n"
                + "   \t<line x1=\"23\" y1=\"34\" x2=\"45\" y2=\"65\" stroke-width=\"3\" stroke=\"lime\" />\n"
                + "   \t<polyline stroke=\"black\" stroke-width=\"6\" points=\"20,20 40,25 60,40\"/>\n"
                + "  </g>\n"
                + "</svg>";

        ISvgParser parser = new ISvgParser();
        parserOutput = parser.parseIsvg(input);
    }

    @Test
    public void checkRectStyleColor() {
        assertEquals(CustomColor.red, parserOutput.get(0).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(0).getStyle().getFill());
        assertEquals(20, parserOutput.get(0).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkTextColor() {
        assertEquals(CustomColor.black, parserOutput.get(1).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(1).getStyle().getFill());
        assertEquals(20, parserOutput.get(1).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkFirstLineColor() {
        assertEquals(CustomColor.red, parserOutput.get(2).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(2).getStyle().getFill());
        assertEquals(20, parserOutput.get(2).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkCircleColor() {
        assertEquals(CustomColor.blue, parserOutput.get(3).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(3).getStyle().getFill());
        assertEquals(30, parserOutput.get(3).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkRect2Color() {
        assertEquals(CustomColor.red, parserOutput.get(4).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(4).getStyle().getFill());
        assertEquals(20, parserOutput.get(4).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkPolygonColor() {
        assertEquals(CustomColor.BLACK, parserOutput.get(5).getStyle().getStroke());
        assertTrue(parserOutput.get(5).getStyle().getFill().isNone());
        assertEquals(4, parserOutput.get(5).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkLineColor() {
        assertEquals(CustomColor.decode("#00FF00"), parserOutput.get(6).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(6).getStyle().getFill());
        assertEquals(3, parserOutput.get(6).getStyle().getStrokeWidth(), 1);
    }

    @Test
    public void checkPolylineColor() {
        assertEquals(CustomColor.black, parserOutput.get(7).getStyle().getStroke());
        assertEquals(CustomColor.green, parserOutput.get(7).getStyle().getFill());
        assertEquals(6, parserOutput.get(7).getStyle().getStrokeWidth(), 1);
    }
}