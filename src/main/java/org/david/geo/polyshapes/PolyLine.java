/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.polyshapes;

import org.david.geo.style.CustomColor;
import org.david.geo.style.Style;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.BasicStroke;

/**
 * This class define all attributes to define a {@code PolyLine}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class PolyLine extends PolyShape {
    /**
     * initialize a poly line.
     *
     * @param s stroke CustomColor
     * @param sw stroke width
     * @param n number of points
     */
    public PolyLine(final CustomColor s, final float sw, final int n) {
        //don't need a background, so no fill CustomColor
        super(new Style(s, null, sw), n);
    }

    /**
     * draw a polyLine in a graphic.
     *
     * @param g graphics component
     */
    @Override
    public void draw(final Graphics g) {
        //create a copy of the graphic
        Graphics2D g2 = (Graphics2D) g.create();
        //transform the shape
        transform(g2);
        if (!getStyle().getStroke().isNone()) {
            if (getStyle().getStroke() != null) {
                //set the CustomColor of the stroke
                g2.setColor(getStyle().getStroke());
                //create a stroke from stroke width
                BasicStroke s2 = new BasicStroke(
                        getStyle().getStrokeWidth(),
                        BasicStroke.CAP_ROUND,
                        BasicStroke.JOIN_ROUND
                );
                //set the stroke
                g2.setStroke(s2);
            }
            //draw the poly line
            //don't need to draw the background because
            //the poly line is not a polygon
            g2.drawPolyline(getX(), getY(), getNumberOfPoints());
        }
        g2.dispose();
    }
}
