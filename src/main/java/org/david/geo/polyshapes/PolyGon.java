/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.polyshapes;

import org.david.geo.style.Style;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.BasicStroke;
import java.awt.Polygon;

/**
 * This class define all attributes to define a {@code PolyGon}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public final class PolyGon extends PolyShape {
    /**
     * initialize a polygon.
     *
     * @param s style
     * @param n number of points
     */
    public PolyGon(final Style s, final int n) {
        super(s, n);
    }

    /**
     * draw a polygon in a graphic.
     *
     * @param g graphics component
     */
    @Override
    public void draw(final Graphics g) {
        //create a copy of the graphic
        Graphics2D g2 = (Graphics2D) g.create();
        //transform the shape
        transform(g2);
        Polygon pol = new Polygon(getX(), getY(), getNumberOfPoints());
        if (getStyle().getFill() != null
            && !getStyle().getFill().isNone()
        ) {
                // background color
                g2.setColor(getStyle().getFill());
                //fill the polygon
                g2.fill(pol);
        }
        // draw the polygon using drawPolygon function
        if (getStyle().getStroke() != null
            && !getStyle().getFill().isNone()
        ) {
                //create a stroke from stroke width
                BasicStroke s = new BasicStroke(
                        getStyle().getStrokeWidth(),
                        BasicStroke.CAP_ROUND,
                        BasicStroke.JOIN_ROUND
                );
                g2.setStroke(s);
                g2.setColor(getStyle().getStroke());
                g2.drawPolygon(pol);

        }
        g2.dispose();
    }
}
