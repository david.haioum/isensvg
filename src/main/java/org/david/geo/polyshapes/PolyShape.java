/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.polyshapes;

import org.david.geo.primitive.Primitive;
import org.david.geo.shapes.Point;
import org.david.geo.style.Style;
import org.david.geo.transform.Transform;

/**
 * This abstract class define all attributes to define a {@code PolyShape}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public abstract class PolyShape extends Primitive {
    /**
     * array of x axis coordinates.
     */
    private int[] x;

    /**
     * array of y axis coordinates.
     */
    private int[] y;

    /**
     * number of points.
     */
    private int nbpt;

    /**
     * current index in the arrays to add points inside.
     */
    private int index = 0;
    /**
     * create a poly shape and initialize the array of point to 0.
     *
     * @param s style
     * @param n number of points
     */
    public PolyShape(
            final Style s,
            final int n
    ) {
        super(s, new Transform(null, 0));
        // x and y are separated because of the method drawPolyLine
        x = new int[n];
        y = new int[n];
        nbpt = n;
    }

    /**
     * add a Coordinate to the array.
     *
     * @param xc x axis coordinate
     * @param yc y axis coordinate
     */
    public void addCoordinate(final int xc, final int yc) {
        if (index < nbpt) {
            x[index] = xc;
            y[index] = yc;
            index++;
        } else {
            index = 0;
        }
    }

    /**
     * get the first point of a polyshape.
     *
     * @return new Point
     */
    public Point getFirstPoint() {
        return new Point(x[0], y[0]);
    }

    /**
     * get the last point of a polyshape.
     *
     * @return new Point
     */
    public Point getLastPoint() {
        return new Point(x[index], y[index]);
    }

    /**
     * get access to the array of points (x axis).
     *
     * @return list of coordinate.
     */
    public int[] getX() {
        return x;
    }

    /**
     * get access to the array of points (y axis).
     *
     * @return list of coordinate.
     */
    public int[] getY() {
        return y;
    }

    /**
     * return the number of coordinate in the list.
     *
     * @return number of coordinate in the list
     */
    public int getNumberOfPoints() {
        return x.length;
    }
}
