/**
 * This package contains all classes related to the rotation or the translation
 * of a {@code Primitive}.
 */
package org.david.geo.transform;
