/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.transform;

import org.david.geo.shapes.Point;

/**
 * This class stores the specific parameters to translate or rotate
 * a {@code Primitive}.
 *
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Transform {
    /**
     * x and y value for translation stored in a {@code Point} class.
     */
    private Point translate;

    /**
     * angle of rotation stored in radian.
     */
    private double theta = 0.0;

    /**
     * Create a {@code Transform} from an angle in degrees and two coordinates
     * x and y stored in a {@code Point}.
     *
     * @param tr coordinates of translation
     * @param angle angle of rotation in degree
     */
    public Transform(final Point tr, final double angle) {
        this.translate = tr;
        //convert the given angle in radian
        this.theta = Math.toRadians(angle);
    }

    /**
     * Create a {@code Transform} from two coordinates
     * x and y stored in a {@code Point} only.
     *
     * @param tr coordinates of translation
     */
    public Transform(final Point tr) {
        this.translate = tr;
    }

    /**
     * Create a {@code Transform} from an angle in degrees.
     *
     * @param angle angle of rotation
     */
    public Transform(final double angle) {
        this.theta = Math.toRadians(angle);
        this.translate = new Point(0, 0);
    }

    /**
     * get translation point value.
     *
     * @return the coordinate of translation as a {@code Point}
     */
    public Point getTranslate() {
        return translate;
    }

    /**
     * set the translation coordinate.
     *
     * @param tr coordinate of translation as a {@code Point}
     */
    public void setTranslate(final Point tr) {
        this.translate = tr;
    }

    /**
     * get angle of rotation.
     *
     * @return rotation angle
     */
    public double getTheta() {
        return theta;
    }

    /**
     * set the angle of rotation.
     *
     * @param value theta in degree
     */
    public void setTheta(final double value) {
        this.theta = value;
    }
}
