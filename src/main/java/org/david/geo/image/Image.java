/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.image;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.david.geo.primitive.Primitive;
import org.david.geo.transform.Transform;
import org.david.geo.shapes.Point;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.io.IOException;

/**
 * This class define all attributes to define a general {@code Image}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Image extends Primitive {
    /**
     * define the LOGGER of image.
     */
    private static final Logger LOGGER = LogManager.getLogger(Image.class);

    /**
     * path to the image.
     */
    private String path = "";

    /**
     * Coordinate.
     */
    private Point origin = null;

    /**
     * construct an image with a path and an origin point.
     *
     * @param p path to the image from resources folder
     * @param o x and y coordinates
     */
    public Image(final String p, final Point o) {
        super(null, new Transform(new Point(0, 0), 0));
        this.path = p;
        this.origin = o;

    }

    /**
     * default constructor.
     */
    public Image() {
        super();
    }

    /**
     * set the new path of the file.
     *
     * @param p new path
     */
    public void setPath(final String p) {
        this.path = p;
    }

    /**
     * set the new origin point.
     *
     * @param p new point
     */
    public void setOrigin(final Point p) {
        this.origin = p;
    }

    /**
     * get origin point.
     *
     * @return origin point
     */
    public Point getOrigin() {
        return origin;
    }

    /**
     * get image path.
     *
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * draw an image in a graphic.
     *
     * @param g graphics component
     */
    @Override
    public void draw(final Graphics g) {
        Graphics g2 = g.create();
        //transform
        g2.translate(
                getTransform().getTranslate().getX(),
                getTransform().getTranslate().getY()
        );

        //creating a buffered image from source
        BufferedImage img = null;
        try {
            //to use a .JAR file later we need to use getResource()
            img = ImageIO.read(getClass().getResource(path));
        } catch (IOException e) {
            LOGGER.error("image not found for " + getClass().getResource(path));
        }
        g2.drawImage(img, origin.getX(), origin.getY(), null);
        g2.dispose();
    }
}
