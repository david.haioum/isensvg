/**
 * This package contains all classes related to drawing.
 * Primitive is the main class of all geometric shapes.
 */
package org.david.geo;
