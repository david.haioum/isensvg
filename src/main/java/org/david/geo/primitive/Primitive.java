/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.primitive;

import org.david.geo.style.CustomColor;
import org.david.geo.style.Style;
import org.david.geo.transform.Transform;

import java.awt.Graphics2D;

/**
 * This abstract class define all attributes to define a {@code Primitive}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public abstract class Primitive implements PrimitiveInterface {
    /**
     * transform object to rotate and translate.
     */
    private Transform transform;

    /**
     * style of the primitive.
     *
     * fill CustomColor
     * stroke CustomColor
     * stroke-width
     */
    private Style style = new Style();

    /**
     * surcharged constructor to initialize a primitive.
     *
     * @param s primitive style
     */
    public Primitive(final Style s) {
        if (s != null) {
            this.style = s;
        }
    }

    /**
     * default constructor.
     */
    public Primitive() {
    }

    /**
     * surcharged constructor to initialize a primitive.
     *
     * @param s primitive style
     * @param t transform properties
     */
    public Primitive(
            final Style s,
            final Transform t
    ) {
        if (s != null) {
            this.style = s;
        }
        this.transform = t;
    }

    /**
     * get the primitive style.
     *
     * @return style of the primitive
     */
    public Style getStyle() {
        return style;
    }

    /**
     * set a new primitive style.
     *
     * @param s style expected
     */
    public void setStyle(final Style s) {
        if (getStyle().isFillEmpty()) {
            if (!s.isFillEmpty()) {
                getStyle().setFill(s.getFill());
            } else {
                getStyle().setFill(CustomColor.BLACK);
            }
        }
        if (getStyle().isStrokeEmpty()) {
            if (!s.isStrokeEmpty()) {
                getStyle().setStroke(s.getStroke());
            } else {
                getStyle().setStroke(CustomColor.black);
            }
        }
        if (getStyle().isStrokeWidthDefault()) {
            if (!s.isStrokeWidthDefault()) {
                getStyle().setStrokeWidth(s.getStrokeWidth());
            } else {
                getStyle().setStrokeWidth(1f);
            }
        }
    }

    /**
     * get transformation properties.
     *
     * @return Transform class
     */
    public Transform getTransform() {
        return transform;
    }

    /**
     * set the transformation properties.
     *
     * @param t object
     */
    public void setTransform(final Transform t) {
        this.transform = t;
    }

    /**
     * transform the 2D graphic.
     *
     * @param g2 2D graphic
     */
    public void transform(final Graphics2D g2) {
        //transform
        if (getTransform().getTranslate() != null) {
            g2.translate(
                    getTransform().getTranslate().getX(),
                    getTransform().getTranslate().getY()
            );
        }
        if (getTransform().getTheta() != 0) {
            g2.rotate(getTransform().getTheta());
        }
    }
}
