/**
 * A primitive is a general abstract class to implement polymorphism in the
 * drawing zone.
 */
package org.david.geo.primitive;
