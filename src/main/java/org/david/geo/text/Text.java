/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.text;

import org.david.geo.primitive.Primitive;
import org.david.geo.shapes.Point;
import org.david.geo.style.CustomColor;
import org.david.geo.style.Style;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Font;

/**
 * This class is used to store all parameters to draw a string (text) in a
 * {@code java.awt.Graphics}.
 * The class extends from primitive in order to implement polymorphism when
 * drawing all the {@code Primitives} on the screen.
 *
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Text extends Primitive {
    /**
     * default font size.
     */
    private static final int DEFAULT_FONT_SIZE = 30;

    /**
     * default font of the text.
     */
    private Font font = new Font("TimesRoman", Font.PLAIN, DEFAULT_FONT_SIZE);

    /**
     * origin coordinate (top-left corner).
     */
    private Point origin;

    /**
     * text value.
     */
    private String value = "";

    /**
     * Create a {@code Text} class with all attributes given.
     *
     * @param val text value to display
     * @param orig origin coordinate (top-left corner)
     * @param textCustomColor text CustomColor
     * @param f font of the text
     */
    public Text(
            final String val,
            final Point orig,
            final CustomColor textCustomColor,
            final Font f
    ) {
        super(new Style(textCustomColor, null, 0.0f));
        this.font = f;
        this.origin = orig;
        this.value = val;
    }

    /**
     * Create a simple {@code Text} class.
     *
     * @param o origin point (top-left corner)
     * @param val value of the string
     */
    public Text(final Point o, final String val) {
        super(new Style(CustomColor.BLACK, null, 0.0f));
        this.origin = o;
        this.value = val;
    }

    /**
     * create a {@code Text} class from the coordinates.
     *
     * @param o origin point (top-left corner)
     */
    public Text(final Point o) {
        super(new Style(CustomColor.BLACK, null, 0.0f));
        this.origin = o;
    }

    /**
     * set the value of the text.
     *
     * @param val text
     */
    public void setValue(final String val) {
        this.value = val;
    }

    /**
     * get the text font.
     *
     * @return font of the text
     */
    public Font getFont() {
        return font;
    }

    /**
     * set the font of the text.
     *
     * @param f new font
     */
    public void setFont(final Font f) {
        this.font = f;
    }

    /**
     * draw a text in a graphic.
     *
     * @param g graphics component
     */
    @Override
    public void draw(final Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        //set the CustomColor
        transform(g2);
        g2.setColor(getStyle().getStroke());
        //set the font, the font weight and the font size
        g2.setFont(font);
        //draw the text at 300, 300
        g2.drawString(value, origin.getX(), origin.getY());
        g2.dispose();
    }
}
