/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.line;

import org.david.geo.primitive.Primitive;
import org.david.geo.style.CustomColor;
import org.david.geo.style.Style;
import org.david.geo.transform.Transform;
import org.david.geo.shapes.Point;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.BasicStroke;

/**
 * This class define all attributes to define a {@code Line}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public final class Line extends Primitive {
    /**
     * first coordinate.
     */
    private Point p1 = null;
    /**
     * second coordinate.
     */
    private Point p2 = null;

    /**
     * initialize a line.
     *
     * @param s stroke CustomColor
     * @param sw stroke width
     * @param pt1 first coordinate
     * @param pt2 second coordinate
     */
    public Line(
            final CustomColor s,
            final float sw,
            final Point pt1,
            final Point pt2
    ) {
        //don't need a background, so no fill CustomColor
        super(new Style(s, null, sw), new Transform(new Point(0, 0), 0));
        p1 = pt1;
        p2 = pt2;
    }

    /**
     * default constructor.
     */
    public Line() {
        super();
    }

    /**
     * set the first point.
     *
     * @param p new point
     */
    public void setP1(final Point p) {
        this.p1 = p;
    }

    /**
     * set the second point.
     *
     * @param p new point
     */
    public void setP2(final Point p) {
        this.p2 = p;
    }

    /**
     * get the first point.
     *
     * @return p1
     */
    public Point getP1() {
        return p1;
    }

    /**
     * get the second point.
     *
     * @return p2
     */
    public Point getP2() {
        return p2;
    }

    /**
     * draw a Line in a graphic.
     *
     * @param g graphics component
     */
    @Override
    public void draw(final Graphics g) {
        //create a copy of the graphic
        Graphics2D g2 = (Graphics2D) g.create();
        //transform
        transform(g2);
        //create a stroke from stroke width
        BasicStroke s2 = new BasicStroke(
                getStyle().getStrokeWidth(),
                BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND
        );
        //set the CustomColor of the strokeA
        if (getStyle().getStroke() != null) {
            g2.setColor(getStyle().getStroke());
        } else {
            g2.setColor(CustomColor.BLACK);
        }
        //set the stroke
        g2.setStroke(s2);
        //draw the line
        //don't need to draw the background because
        //the line is not a polygon
        g2.drawLine(p1.getX(), p1.getY(), p2.getX(), p2.getY());

        g2.dispose();

    }
}

