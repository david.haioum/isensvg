/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.style;

/**
 * This class provide all the attributes and methods
 * from {@code java.awt.Color} and add a new attributes "isNone" to indicate
 * whether the Color is defnied or not in the {@code IsvgParser}.
 *
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class CustomColor extends java.awt.Color {
    /**
     * indicate whether the Color is defnied or not in the {@code IsvgParser}.
     */
    private boolean isNone;

    /**
     * The CustomColor white.  In the default sRGB space.
     */
    public static final CustomColor white = new CustomColor(255, 255, 255);

    /**
     * The CustomColor white.  In the default sRGB space.
     
     */
    public static final CustomColor WHITE = white;

    /**
     * The CustomColor light gray.  In the default sRGB space.
     */
    public static final CustomColor lightGray = new CustomColor(192, 192, 192);

    /**
     * The CustomColor light gray.  In the default sRGB space.
     
     */
    public static final CustomColor LIGHT_GRAY = lightGray;

    /**
     * The CustomColor gray.  In the default sRGB space.
     */
    public static final CustomColor gray      = new CustomColor(128, 128, 128);

    /**
     * The CustomColor gray.  In the default sRGB space.
     
     */
    public static final CustomColor GRAY = gray;

    /**
     * The CustomColor dark gray.  In the default sRGB space.
     */
    public static final CustomColor darkGray  = new CustomColor(64, 64, 64);

    /**
     * The CustomColor dark gray.  In the default sRGB space.
     
     */
    public static final CustomColor DARK_GRAY = darkGray;

    /**
     * The CustomColor black.  In the default sRGB space.
     */
    public static final CustomColor black     = new CustomColor(0, 0, 0);

    /**
     * The CustomColor black.  In the default sRGB space.
     
     */
    public static final CustomColor BLACK = black;

    /**
     * The CustomColor red.  In the default sRGB space.
     */
    public static final CustomColor red       = new CustomColor(255, 0, 0);

    /**
     * The CustomColor red.  In the default sRGB space.
     
     */
    public static final CustomColor RED = red;

    /**
     * The CustomColor pink.  In the default sRGB space.
     */
    public static final CustomColor pink      = new CustomColor(255, 175, 175);

    /**
     * The CustomColor pink.  In the default sRGB space.
     */
    public static final CustomColor PINK = pink;

    /**
     * The CustomColor orange.  In the default sRGB space.
     */
    public static final CustomColor orange    = new CustomColor(255, 200, 0);

    /**
     * The CustomColor orange.  In the default sRGB space.
     */
    public static final CustomColor ORANGE = orange;

    /**
     * The CustomColor yellow.  In the default sRGB space.
     */
    public static final CustomColor yellow    = new CustomColor(255, 255, 0);

    /**
     * The CustomColor yellow.  In the default sRGB space.
     */
    public static final CustomColor YELLOW = yellow;

    /**
     * The CustomColor green.  In the default sRGB space.
     */
    public static final CustomColor green     = new CustomColor(0, 255, 0);

    /**
     * The CustomColor green.  In the default sRGB space.
     */
    public static final CustomColor GREEN = green;

    /**
     * The CustomColor magenta.  In the default sRGB space.
     */
    public static final CustomColor magenta   = new CustomColor(255, 0, 255);

    /**
     * The CustomColor magenta.  In the default sRGB space.
     */
    public static final CustomColor MAGENTA = magenta;

    /**
     * The CustomColor cyan.  In the default sRGB space.
     */
    public static final CustomColor cyan      = new CustomColor(0, 255, 255);

    /**
     * The CustomColor cyan.  In the default sRGB space.
     */
    public static final CustomColor CYAN = cyan;

    /**
     * The CustomColor blue.  In the default sRGB space.
     */
    public static final CustomColor blue      = new CustomColor(0, 0, 255);

    /**
     * The CustomColor blue.  In the default sRGB space.
     */
    public static final CustomColor BLUE = blue;

    /**
     * Create a Custom color.
     *
     * @param r red [0-255]
     * @param g green [0-255]
     * @param b blue [0-255]
     */
    public CustomColor(
            final int r,
            final int g,
            final int b
    ) {
        super(r, g, b);
        this.isNone = false;
    }

    /**
     * Create a Custom color with isNone.
     *
     * @param r red [0-255]
     * @param g green [0-255]
     * @param b blue [0-255]
     * @param n isNone
     */
    public CustomColor(
            final int r,
            final int g,
            final int b,
            final boolean n
    ) {
        super(r, g, b);
        this.isNone = n;
    }

    /**
     * Create a Custom color from boolean.
     *
     * @param none isNone
     */
    public CustomColor(final boolean none) {
        super(0);
        this.isNone = none;
    }

    /**
     * check whether the color is none or not.
     *
     * @return true if the color is "none", false if not empty
     */
    public boolean isNone() {
        return isNone;
    }

    /**
     * set the new value of isNone.
     *
     * @param none new value of isNone
     */
    public void setNone(final boolean none) {
        isNone = none;
    }

    /**
     * decode a CustomColor from a Hexadecimal String
     *
     * @param nm string to decode
     * @return new custom color from the string
     */
    public static CustomColor decode(final String nm) {
        int i = Integer.decode(nm);
        return new CustomColor((i >> 16) & 0xFF, (i >> 8) & 0xFF, i & 0xFF);
    }

    /**
     * compare two CustomColor.
     *
     * @param obj second CustomColor
     * @return true if the two objects are equals
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * return hashcode of CustomColor.
     *
     * @return hashcode number
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
