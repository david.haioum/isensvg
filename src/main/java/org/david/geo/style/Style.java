/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.style;

/**
 * This class store parameters to indicate the stroke color, the stroke width
 * and the fill color of a {@code Primitive}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Style {
    /**
     * stroke CustomColor.
     */
    private CustomColor stroke = null;
    /**
     * fill CustomColor.
     */
    private CustomColor fill = null;
    /**
     * stroke width.
     */
    private float strokeWidth = 0.0f;

    /**
     * default constructor.
     */
    public Style() {
    }

    /**
     * create a new Style.
     *
     * @param s stroke CustomColor
     * @param f fill CustomColor
     * @param stw stroke width
     */
    public Style(
            final CustomColor s,
            final CustomColor f,
            final float stw
    ) {
        this.stroke = s;
        this.fill = f;
        this.strokeWidth = stw;
    }

    /**
     * check if the style is empty.
     *
     * @return boolean
     */
    public boolean isStrokeEmpty() {
        return (getStroke() == null);
    }

    /**
     * check if the fill CustomColor is empty.
     *
     * @return true or false
     */
    public boolean isFillEmpty() {
        return (getFill() == null);
    }

    /**
     * stroke is empty ?
     *
     * @return true or false
     */
    public boolean isStrokeWidthDefault() {
        return Float.compare(getStrokeWidth(), 0.0f) == 0;
    }

    /**
     * get the stroke CustomColor.
     *
      * @return stroke CustomColor
     */
    public CustomColor getStroke() {
        return stroke;
    }

    /**
     * get the stroke CustomColor.
     *
     * @param s stroke CustomColor
     */
    public void setStroke(final CustomColor s) {
        this.stroke = s;
    }

    /**
     * get the fill CustomColor.
     *
     * @return fill CustomColor
     */
    public CustomColor getFill() {
        return fill;
    }

    /**
     * set the fill CustomColor.
     *
     * @param f fill CustomColor
     */
    public void setFill(final CustomColor f) {
        this.fill = f;
    }

    /**
     * get the stroke width.
     *
     * @return stroke width
     */
    public float getStrokeWidth() {
        return strokeWidth;
    }

    /**
     * set the stroke width.
     *
     * @param stw stroke width
     */
    public void setStrokeWidth(final float stw) {
        this.strokeWidth = stw;
    }
}
