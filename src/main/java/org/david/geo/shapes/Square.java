/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.shapes;

import org.david.geo.style.Style;


/**
 * This class define a {@code Square} based on a {@code Rectangle}
 * that extends from {@code Shape}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public final class Square extends Rectangle {
    /**
     * how many times the width is multiplied to get the perimeter
     * avoid magic number.
     */
    private static final int COUNT = 4;

    /**
     * create a square with full parameters.
     *
     * @param s style
     * @param op origin point
     * @param side side of the square
     */
    public Square(
            final Style s,
            final Point op,
            final int side
    ) {
        super(s, op, side, side);
    }

    /**
     * default constructor.
     */
    public Square() {
        super();
    }

    /**
     * get the side of square.
     *
     * @return side from rectangle superclass
     */
    public double getSide() {
        return super.getWidth();
    }

    /**
     * set the side of the square.
     *
     * @param side side f the rectangle
     */
    public void setSide(final int side) {
        super.setWidth(side);
        super.setHeight(side);
    }

    /**
     * compute the perimeter of the square.
     *
     * @return computed perimeter
     */
    @Override
    public double getPerimeter() {
        return (double) super.getWidth() * COUNT;
    }

    /**
     * compute the surface of the square.
     *
     * @return computed surface
     */
    @Override
    public double getSurface() {
        return super.getWidth() * (double) super.getWidth();
    }


}
