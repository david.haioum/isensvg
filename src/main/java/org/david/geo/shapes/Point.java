/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.shapes;

/**
 * This class define all attributes to define a {@code Point}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public final class Point {
    /**
     * x axis coordinate.
     */
    private int xp;
    /**
     * y axis coordinate.
     */
    private int yp;

    /**
     * create a point from coordinate.
     *
     * @param x x axis coordinate
     * @param y y axis coordinate
     */
    public Point(final int x, final int y) {
        this.xp = x;
        this.yp = y;
    }

    /**
     *
     * @return x
     */
    public int getX() {
        return xp;
    }

    /**
     *
     * @return y
     */
    public int getY() {
        return yp;
    }

    /**
     * set the x coordinate.
     *
     * @param x new value
     */
    public void setX(final int x) {
        this.xp = x;
    }

    /**
     * set the y coordinate.
     *
     * @param y new value
     */
    public void setY(final int y) {
        this.yp = y;
    }
}
