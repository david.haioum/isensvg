/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.shapes;

import org.david.geo.style.Style;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.Ellipse2D;

/**
 * This class define all attributes to define an {@code Ellipse}
 * and extends from {@code Shape}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Ellipse extends Shape {
    /**
     * small axis.
     */
    private int smallAxis = 0;
    /**
     * big axis.
     */
    private int bigAxis = 0;

    /**
     * create an ellipse with full arguments.
     *
     * @param s style
     * @param op origin point
     * @param w width
     * @param h height
     */
    public Ellipse(
            final Style s,
            final Point op,
            final int w,
            final int h
    ) {
        super(s, op);
        this.smallAxis = h;
        this.bigAxis = w;
    }

    /**
     * default constructor.
     */
    public Ellipse() {
        super();
    }

    /**
     * get small axis of the ellipse.
     *
     * @return small axis of the ellipse
     */
    public int getSmallAxis() {
        return smallAxis;
    }

    /**
     * get big axis of the ellipse.
     *
     * @return big axis of the ellipse
     */
    public int getBigAxis() {
        return bigAxis;
    }

    /**
     * set the samll axis of the ellipse.
     *
     * @param smallAxisWanted small axis of the ellipse
     */
    public void setSmallAxis(final int smallAxisWanted) {
        this.smallAxis = smallAxisWanted;
    }

    /**
     * set the big axis of the ellipse.
     *
     * @param bigAxisWanted big axis value
     */
    public void setBigAxis(final int bigAxisWanted) {
        this.bigAxis = bigAxisWanted;
    }

    /**
     * compute the perimeter of an ellipse.
     *
     * @return computed perimeter
     */
    public double getPerimeter() {
        double bigAxis2 = (double) bigAxis * (double) bigAxis;
        double rootContent = bigAxis2 + (smallAxis * smallAxis);
        return 2 * Math.PI * Math.sqrt(rootContent / 2.0f);
    }

    /**
     * compute the surface of an ellipse.
     *
     * @return computed surface
     */
    public double getSurface() {
        return Math.PI * smallAxis * bigAxis;
    }

    /**
     * set the bounding box of the Ellipse.
     *
     * @param widthBB width of the bounding box
     * @param heightBB height of the bounding box
     */
    @Override
    public void setBoundingBox(final int widthBB, final int heightBB) {
        this.smallAxis = widthBB;
        this.bigAxis = heightBB;
    }

    /**
     * draw an ellipse in a graphic.
     *
     * @param g graphic component to draw the ellipse.
     */
    @Override
    public void draw(final Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        //transform the shape
        transform(g2);
        Ellipse2D ell = new Ellipse2D.Double(
                getOrigin().getX(),
                getOrigin().getY(),
                getSmallAxis(),
                getBigAxis()
        );
        //draw the shape
        super.fillAndDraw(g2, ell);
        //free
        g2.dispose();
    }

    /**
     * set real coordinate for an origin point given.
     *
     * the origin point given is the real center of the circle
     * the real point is the top-left corner point
     */
    public void setRealCoordinates() {
        setOrigin(new Point(
                getOrigin().getX() - getBigAxis(),
                getOrigin().getY() - getBigAxis()
        ));
    }
}
