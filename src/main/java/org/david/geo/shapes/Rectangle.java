/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.shapes;

import org.david.geo.style.Style;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.RoundRectangle2D;

/**
 * This class define all attributes to define a {@code Rectangle}
 * and extends from {@code Shape}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Rectangle extends Shape {
    /**
     * height.
     */
    private int height;
    /**
     * width.
     */
    private int width;

    /**
     * bounds width.
     */
    private int rx = 0;

    /**
     * bounds height.
     */
    private int ry = 0;

    /**
     * create a rectangle with full parameters.
     *
     * @param s style
     * @param op origin point
     * @param h height
     * @param w width
     */
    public Rectangle(
            final Style s,
            final Point op,
            final int h,
            final int w
    ) {
        super(s, op);
        this.height = h;
        this.width = w;
    }

    /**
     * set the bound of the bounded rectangle.
     *
     * @param w bound width
     * @param h bound height
     */
    public void setBound(final int w, final int h) {
        rx = w;
        ry = h;
    }

    /**
     * get bound width.
     *
     * @return bound width
     */
    public int getRx() {
        return rx;
    }

    /**
     * get bound height.
     *
     * @return bound height
     */
    public int getRy() {
        return ry;
    }

    /**
     * default constructor.
     */
    public Rectangle() {
        super();
    }

    /**
     * get the height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * get the width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * set the height.
     *
     * @param h height expected
     */
    public void setHeight(final int h) {
        this.height = h;
    }

    /**
     * set the width.
     *
     * @param w width expected
     */
    public void setWidth(final int w) {
        this.width = w;
    }

    /**
     * compute the surface of the rectangle.
     *
     * @return computed surface
     */
    public double getSurface() {
        return (double) width * (double) height;
    }

    /**
     * set the bounding box of the rectangle.
     *
     * @param widthBB width of the bounding box
     * @param heightBB height of the bounding box
     */
    @Override
    public void setBoundingBox(final int widthBB, final int heightBB) {
        this.height = heightBB;
        this.width = widthBB;
    }

    /**
     * compute the perimeter of the rectangle.
     *
     * @return computed perimeter
     */
    public double getPerimeter() {
        return (double) (2 * width + 2 * height);
    }

    /**
     * draw a rectangle in a graphic.
     *
     * @param g graphic component to draw the shapes.
     */
    @Override
    public void draw(final Graphics g) {
        //create a copy of graphics to do what we want with
        //this allow us to save the current transformation parameters
        Graphics2D g2 = (Graphics2D) g.create();
        //transform the shape
        transform(g2);
        //create a bounded rectangle
        RoundRectangle2D r = new RoundRectangle2D.Double(
                getOrigin().getX(),
                getOrigin().getY(),
                getWidth(),
                getHeight(),
                getRx(),
                getRy()
        );
        //draw the shape
        super.fillAndDraw(g2, r);
        //release the graphics
        g2.dispose();
    }
}
