/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.shapes;

import org.david.geo.primitive.Primitive;
import org.david.geo.style.Style;
import org.david.geo.transform.Transform;

import java.awt.Graphics2D;
import java.awt.BasicStroke;

/**
 * This abstract class define all attributes to define a {@code Shape}
 * and extends from {@code Primitive}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public abstract class Shape extends Primitive {
    /**
     * origin point of the shape.
     */
    private Point origin;

    /**
     * create a shape from an origin point.
     *
     * @param s style
     * @param op origin point
     */
    public Shape(final Style s, final Point op) {
        super(s, new Transform(null, 0));
        this.origin = op;
    }

    /**
     * default constructor.
     */
    public Shape() {
        super();
        origin = null;
    }

    /**
     * get the origin point.
     *
     * @return Point returned
     */
    public Point getOrigin() {
        return origin;
    }

    /**
     * set new origin point.
     *
     * @param p origin point
     */
    public void setOrigin(final Point p) {
        this.origin = p;
    }

    /**
     * set the bounding box of the shape. (abstract)
     *
     * @param widthBB width of the bounding box
     * @param heightBB height of the bounding box
     */
    public abstract void setBoundingBox(int widthBB, int heightBB);

    /**
     * get the perimeter of the shape.
     *
     * @return perimeter computed
     */
    public abstract double getPerimeter();

    /**
     * get the surface of the shape.
     *
     * @return surface computed
     */
    public abstract double getSurface();

    /**
     * fill a {@code Shape} and draw the stroke of
     * a {@code java.awt.Rectangle2D} or a {@code java.awt.Ellipse2D}.
     *
     * @param g2 {@code Graphics2D} class
     * @param s shape to draw
     */
    public void fillAndDraw(final Graphics2D g2, final java.awt.Shape s) {
        //fill the rectangle red
        if (getStyle().getFill() != null
            && !getStyle().getFill().isNone()
        ) {
                g2.setColor(getStyle().getFill()); //background color
                //draw it first
                g2.fill(s);
        }

        if (getStyle().getStroke() != null) {
            //change the stroke width here
            BasicStroke stroke = new BasicStroke(
                    getStyle().getStrokeWidth(),
                    BasicStroke.CAP_ROUND,
                    BasicStroke.JOIN_ROUND
            );
            g2.setStroke(stroke);
            //set color to black
            g2.setColor(getStyle().getStroke()); //stroke color
            //draw the black stroke after /!\
            g2.draw(s);
        }
    }
}
