/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.geo.shapes;

import org.david.geo.style.Style;

/**
 * This class define all attributes to define a {@code Circle}
 * and extends from {@code Ellipse}.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class Circle extends Ellipse {
    /**
     * create a circle with all arguments.
     *
     * @param s style
     * @param op origin point
     * @param radius radius of the circle
     */
    public Circle(
            final Style s,
            final Point op,
            final int radius
    ) {
        super(s, op, radius, radius);
    }

    /**
     * default constructor.
     */
    public Circle() {
        super();
    }

    /**
     * set the axis of the circle, so the radius.
     *
     * @param r axis means the radius
     */
    public void setRadius(final int r) {
        super.setBigAxis(r);
        super.setSmallAxis(r);
    }

    /**
     * compute the surface of the circle.
     *
     * @return computed surface
     */
    @Override
    public double getSurface() {
        return 2 * Math.PI * super.getBigAxis();
    }

    /**
     * get the perimeter of the circle.
     *
     * @return computed perimeter
     */
    @Override
    public double getPerimeter() {
        int radius = super.getSmallAxis();
        return Math.PI * radius * radius;
    }

    /**
     * get the radius of the circle.
     *
     * @return radius
     */
    public int getRadius() {
        return super.getBigAxis();
    }
}
