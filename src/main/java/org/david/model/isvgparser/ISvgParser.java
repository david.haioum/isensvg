/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.model.isvgparser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.david.geo.line.Line;
import org.david.geo.polyshapes.PolyGon;
import org.david.geo.polyshapes.PolyLine;
import org.david.geo.primitive.Primitive;
import org.david.geo.image.Image;
import org.david.geo.shapes.Circle;
import org.david.geo.shapes.Ellipse;
import org.david.geo.shapes.Point;
import org.david.geo.shapes.Rectangle;
import org.david.geo.style.Style;
import org.david.geo.text.Text;
import org.david.geo.transform.Transform;

import org.w3c.dom.DOMException;

import org.xml.sax.XMLReader;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import javax.xml.transform.TransformerFactoryConfigurationError;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;

import java.util.ArrayList;

/**
 * This class is the official ".isvg" parser based on XML and SAX API.
 * from a given input, a list of {@code Primitive} is returned.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class ISvgParser extends DefaultHandler implements Serializable {
    /**
     * serializable UID.
     */
    private static final long serialVersionUID = 5546581213676910281L;

    /**
     * string that define the DOCTYPE of the file
     * this will be used to remove it in a document later because
     * the SAX parser can not read it.
     */
    private static final String DOCTYPE_LINE_TO_REMOVE = "<!DOCTYPE svg PUBLIC"
            + " \"-//W3C//DTD SVG 20010904//EN\" \n"
            + "  \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">";

    /**
     * this boolean is true if there is characters after a XML object.
     */
    private boolean bText = false;
    /**
     * is true if a "g" XML object is found in the document
     * in order to set the translate coordinate and the angle of rotation
     * to the concerned {@code Primitive}.
     */
    private boolean bTransform = false;

    /**
     * LOGGER of the class.
     */
    private static final Logger LOGGER = LogManager.getLogger(ISvgParser.class);

    /**
     * store the values of translate coordinate and the angle of rotation.
     * This class is given to the according temporary Primitive.
     */
    private Transform transform = null;

    /**
     * useful string buffer to store the characters after a XML object.
     */
    private StringBuilder buffer = null;

    /**
     * temporary {@code Text} class.
     */
    private Text tempText = null;
    /**
     * temporary {@code Rectangle} class.
     */
    private Rectangle tmpRectangle = null;
    /**
     * temporary {@code Circle} class.
     */
    private Circle tmpCircle = null;
    /**
     * temporary {@code Line} class.
     */
    private Line tmpLine = null;
    /**
     * temporary {@code PolyLine} class.
     */
    private PolyLine tmpPolyLine = null;
    /**
     * temporary {@code PolyGon} class.
     */
    private PolyGon tmpPolyGon = null;
    /**
     * temporary {@code Image} class.
     */
    private Image tmpIsenMagic = null;
    /**
     * temporary {@code Ellipse} class.
     */
    private Ellipse tmpEllipse = null;
    /**
     * temporary {@code Style} class.
     */
    private Style tmpStyle = new Style(null, null, 0.0f);

    /**
     * error handler of the SAX parser. all the errors will be treated
     * by this error handler.
     */
    private ISvgErrorHandler errorHandler = new ISvgErrorHandler();

    /**
     * array of primitives parsed from isvg file.
     */
    private ArrayList<Primitive> arrayOfPrimitives;

    /**
     * parse the string input i an array of primitives.
     *
     * @param input isvg string input
     * @return the list of primitives
     */
    public ArrayList<Primitive> parseIsvg(final String input) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader xr = parser.getXMLReader();
            xr.setErrorHandler(errorHandler);
            ISvgParser isvgParser = new ISvgParser();
            xr.setContentHandler(isvgParser);
            InputSource targetFile = new InputSource(new StringReader(input));
            xr.parse(targetFile);
            LOGGER.debug("parsing success");
            return isvgParser.arrayOfPrimitives;
        } catch (DOMException e) {
            LOGGER.error("DOM operation impossible " + e.getMessage());
        } catch (ParserConfigurationException e) {
            LOGGER.error("configuration failed");
        } catch (TransformerFactoryConfigurationError e) {
            LOGGER.error("can not find transformer factory configuration");
        } catch (SAXException e) {
            LOGGER.error("parser error :" + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("wrong input : " + input);
        }
        return new ArrayList<>();
    }

    /**
     * start of the document.
     *
     * @throws SAXException exceptions thrown
     */
    @Override
    public void startDocument() throws SAXException {
        arrayOfPrimitives = new ArrayList<>();
        LOGGER.info("[LOG] start parsing");
    }

    /**
     * end of the document.
     *
     * @throws SAXException exceptions thrown
     */
    @Override
    public void endDocument() throws SAXException {
        LOGGER.info("[LOG] end parsing");
    }

    /**
     * read an element from start.
     *
     * @param uri uri name
     * @param localName local name
     * @param qName name of the attribut in the XML syntax
     * @param atts attributes given in XML
     * @throws SAXException SAX exception
     */
    @Override
    public void startElement(
            final String uri,
            final String localName,
            final String qName,
            final Attributes atts
    ) throws SAXException {
        if (qName.equals("rect")) {
            tmpRectangle = Parse.parseRectangle(atts);
        } else if (qName.equals("isen_magic")) {
            tmpIsenMagic = Parse.parseIsenMagic(atts);
        } else if (qName.equals("circle")) {
            tmpCircle = Parse.parseCircle(atts);
        } else if (qName.equals("text")) {
            bText = true;
            tempText = new Text(
                new Point(
                        Parse.parseInt("x", atts),
                        Parse.parseInt("y", atts)
                ),
                ""
            );
        } else if (qName.equals("g")) {
            bTransform = true;
            transform = Parse.parseTransform(
                    Parse.getAttributeValueOf("transform", atts)
            );
            tmpStyle = new Style(
                    Parse.parseColor(
                        Parse.getAttributeValueOf("stroke", atts)
                    ),
                    Parse.parseColor(
                        Parse.getAttributeValueOf("fill", atts)
                    ),
                    Parse.parseFloat("stroke-width", atts)
            );
        } else if (qName.equals("ellipse")) {
            tmpEllipse = Parse.parseEllipse(atts);
        } else if (qName.equals("line")) {
            tmpLine = Parse.parseLine(atts);
        } else if (qName.equals("polyline")) {
            tmpPolyLine = Parse.parsePolyLine(atts);
        } else if (qName.equals("polygon")) {
            tmpPolyGon = Parse.parsePolyGon(atts);
        }

        buffer = new StringBuilder();
    }

    /**
     * get the arrayOFPrimitive to send to the drawing zone (vue).
     *
     * @return list of primitives
     */
    public ArrayList<Primitive> getArrayOfPrimitives() {
        return arrayOfPrimitives;
    }

    /**
     * end of an element.
     *
     * @param uri uri name
     * @param localName local name
     * @param qName name of the attribut in the XML syntax
     * @throws SAXException SAX exception
     */
    @Override
    public void endElement(
            final String uri,
            final String localName,
            final String qName
    ) throws SAXException {
        if (bText) {
            if (bTransform) {
                tempText.setTransform(transform);
                tempText.setStyle(tmpStyle);
            }
            tempText.setValue(buffer.toString());
            arrayOfPrimitives.add(tempText);
            bText = false;
            buffer.setLength(0);
        }
        if (qName.equals("g")) {
            bTransform = false;
        }

        if (qName.equals("polygon")) {
            if (bTransform) {
                tmpPolyGon.setTransform(transform);
                tmpPolyGon.setStyle(tmpStyle);
            }
            arrayOfPrimitives.add(tmpPolyGon);
        }
        if (qName.equals("polyline")) {
            if (bTransform) {
                tmpPolyLine.setTransform(transform);
                tmpPolyLine.setStyle(tmpStyle);
            }
            arrayOfPrimitives.add(tmpPolyLine);
        }
        if (qName.equals("circle")) {
            if (bTransform) {
                tmpCircle.setTransform(transform);
                tmpCircle.setStyle(tmpStyle);
            }
            arrayOfPrimitives.add(tmpCircle);
        }
        if (qName.equals("isen_magic")) {
            if (bTransform) {
                tmpIsenMagic.setTransform(transform);
            }
            arrayOfPrimitives.add(tmpIsenMagic);
        }
        if (qName.equals("ellipse")) {
            if (bTransform) {
                tmpEllipse.setTransform(transform);
                tmpEllipse.setStyle(tmpStyle);
            }
            arrayOfPrimitives.add(tmpEllipse);
        }
        if (qName.equals("rect")) {

            if (bTransform) {
                tmpRectangle.setTransform(transform);
                tmpRectangle.setStyle(tmpStyle);
            }
            arrayOfPrimitives.add(tmpRectangle);
        }
        if (qName.equals("line")) {
            if (bTransform) {
                tmpLine.setTransform(transform);
                tmpLine.setStyle(tmpStyle);
            }
            arrayOfPrimitives.add(tmpLine);
        }
    }

    /**
     * read characters after the end of the XML object.
     *
     * @param ch array of char
     * @param start start position
     * @param length length
     * @throws SAXException SAX exception
     */
    @Override
    public void characters(
            final char[] ch,
            final int start,
            final int length
    ) throws SAXException {
        if (bText) {
            buffer.append(new String(ch, start, length));
        }
    }

    /**
     * get the {@code IsvgErrorHandler} object.
     *
     * @return the error handler object
     */
    public ISvgErrorHandler getErrorHandler() {
        return errorHandler;
    }
}
