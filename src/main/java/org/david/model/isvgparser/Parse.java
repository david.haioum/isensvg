/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.model.isvgparser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.david.geo.image.Image;
import org.david.geo.line.Line;
import org.david.geo.polyshapes.PolyGon;
import org.david.geo.polyshapes.PolyLine;
import org.david.geo.shapes.Circle;
import org.david.geo.shapes.Ellipse;
import org.david.geo.shapes.Point;
import org.david.geo.shapes.Rectangle;
import org.david.geo.style.CustomColor;
import org.david.geo.style.Style;
import org.david.geo.transform.Transform;
import org.xml.sax.Attributes;

import java.util.ArrayList;

/**
 * This utility class is used to parse a {@code Primitive} from a ".isvg" / XML
 * file.
 *
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public final class Parse {
    /**
     * not found string constant.
     */
    private static final String NOT_FOUND = "not found";
    /**
     * stroke string constant.
     */
    private static final String STROKE = "stroke";
    /**
     * stroke-width string constant.
     */
    private static final String STROKE_WIDTH = "stroke-width";
    /**
     * used in logger.
     */
    private static final String GIVES = " gives ";

    /**
     * LOGGER of the class.
     */
    private static final Logger LOGGER = LogManager.getLogger(Parse.class);

    /**
     * private constructor (utility class).
     */
    private Parse() {
    }

    /**
     * parse transform xml attributes in Transform object.
     *
     * @param tr string that contains attribute
     * @return new Transform
     */
    static Transform parseTransform(final String tr) {
        //initialize return attributes.
        float x = 0;
        float y = 0;
        float theta = 0;
        //split the main string separated by " "
        String[] content = tr.split(" ");
        // /!\ " " is counted in content.length

        //for all substring
        for (String s : content) {
            //get the first coordinate
            if (s.contains("translate")) {
                //split string : "translate(%f"
                //get the right side of this string
                x = Float.parseFloat((s.split("\\("))[1]);
            } else if (s.contains("rotate")) {
                //split string : "rotate(%d)"
                //get the left side -> return "%f)"
                String rotate = (s.split("\\("))[1];
                //get the right part of "%d)"
                theta = Float.parseFloat(((rotate.split("\\)"))[0]));
            } else if (s.contains(")")) {
                //get the left part of %f) extracted from translate(%f %f)
                y = Float.parseFloat(((s.split("\\)"))[0]));
            }
        }
        //return a new Transform object
        return new Transform(
                new Point(
                        Math.round(x),
                        Math.round(y)
                ),
                Math.round(theta)
        );
    }

    /**
     * parse a polyGon xml object to PolyGon object.
     *
     * @param atts attributes
     * @return new PolyLine
     */
    static PolyGon parsePolyGon(final Attributes atts) {
        PolyGon pl = null;
        ArrayList<Point> tableOfPoints;

        String points = getAttributeValueOf("points", atts);
        if (!(points.equals(NOT_FOUND))) {
            tableOfPoints = parsePolyPoint(points);
            pl = new PolyGon(
                    new Style(
                        parseColor(
                                getAttributeValueOf(STROKE, atts)
                        ),
                        parseColor(
                                getAttributeValueOf("fill", atts)
                        ),
                        parseFloat(STROKE_WIDTH, atts)
                    ),
                    tableOfPoints.size()
            );

            for (Point p : tableOfPoints) {
                pl.addCoordinate(
                        p.getX(),
                        p.getY()
                );
            }
        }
        return pl;
    }

    /**
     * search in the attributes array a specific attribute from it name.
     *
     * @param name searched attribute
     * @param atts array of attributes
     * @return value of attribute as String
     */
    static String getAttributeValueOf(
            final String name,
            final Attributes atts
    ) {
        String result = NOT_FOUND;
        for (int i = 0; i < atts.getLength(); i++) {
            String attName = atts.getLocalName(i);
            if (attName.equals(name)) {
                result = atts.getValue(i);
            }
        }
        return result;
    }

    /**
     * parse a poly point from string.
     *
     * @param points string of points
     * @return new list of points
     */
    static ArrayList<Point> parsePolyPoint(final String points) {
        ArrayList<Point> tableOfPoints = new ArrayList<>();

        String[] tabPoints = points.split(" ");
        for (String s : tabPoints) {
            String[] coord = s.split(",");
            if (coord.length == 2) {
                float x = Float.parseFloat(coord[0]);
                float y = Float.parseFloat(coord[1]);

                tableOfPoints.add(new Point(Math.round(x), Math.round(y)));
            }
        }
        return tableOfPoints;
    }

    /**
     * parse a polyline xml object to PolyLine object.
     *
     * @param atts attributes
     * @return new PolyLine
     */
    static PolyLine parsePolyLine(final Attributes atts) {
        PolyLine pl = null;
        ArrayList<Point> tableOfPoints;

        String points = getAttributeValueOf("points", atts);
        if (!(points.equals(NOT_FOUND))) {
            tableOfPoints = parsePolyPoint(points);

            pl = new PolyLine(
                    parseColor(
                            getAttributeValueOf(STROKE, atts)
                    ),
                    parseFloat(STROKE_WIDTH, atts),
                    tableOfPoints.size()
            );

            for (Point p : tableOfPoints) {
                pl.addCoordinate(
                        p.getX(),
                        p.getY()
                );
            }
        }
        return pl;
    }

    /**
     * parse a line xml object to a Line object.
     *
     * @param atts attributes
     * @return new Line
     */
    static Line parseLine(final Attributes atts) {
        return new Line(
                parseColor(getAttributeValueOf(STROKE, atts)),
                parseFloat(STROKE_WIDTH, atts),
                parsePoint("x1", "y1", atts),
                parsePoint("x2", "y2", atts)
        );
    }

    /**
     * parse an isen_magic xml object to an Image.
     *
     * @param atts attributes
     * @return new Image
     */
    static Image parseIsenMagic(final Attributes atts) {
        return new Image(
                "/isen.jpg",
                parsePoint("x", "y", atts)
        );
    }

    /**
     * parse an ellipse xml object to an Ellipse object.
     *
     * @param atts attributes
     * @return new Ellipse
     */
    static Ellipse parseEllipse(final Attributes atts) {
        return new Ellipse(
                new Style(
                    parseColor(
                            getAttributeValueOf(STROKE, atts)
                    ),
                    parseColor(
                            getAttributeValueOf("fill", atts)
                    ),
                    parseFloat(STROKE_WIDTH, atts)
                ),
                parsePoint("cx", "cy", atts),
                parseInt("rx", atts),
                parseInt("ry", atts)
        );
    }

    /**
     * parse a circle svg object from xml to Circle object.
     *
     * @param atts attributes
     * @return new Circle object
     */
    static Circle parseCircle(final Attributes atts) {
        Circle c = new Circle(
                new Style(
                    parseColor(
                            getAttributeValueOf(STROKE, atts)),
                    parseColor(
                            getAttributeValueOf("fill", atts)),
                    parseFloat(STROKE_WIDTH, atts)
                ),
                new Point(
                        parseInt("cx", atts),
                        parseInt("cy", atts)
                ),
                parseInt("r", atts)
        );
        c.setRealCoordinates();
        return c;
    }

    /**
     * parse xml object to Rectangle object.
     *
     * @param atts attributes
     * @return new Rectangle
     */
    static Rectangle parseRectangle(final Attributes atts) {
        Rectangle r = new Rectangle(
                new Style(
                    parseColor(getAttributeValueOf(STROKE, atts)),
                    parseColor(getAttributeValueOf("fill", atts)),
                    parseFloat(STROKE_WIDTH, atts)
                ),
                new Point(
                        parseInt("x", atts),
                        parseInt("y", atts)
                ),
                parseInt("height", atts),
                parseInt("width", atts)
        );
        int rx = 0;
        int ry = 0;
        String boundX = getAttributeValueOf("rx", atts);
        String boundY = getAttributeValueOf("ry", atts);
        if (!(boundX.equals(NOT_FOUND))) {
            rx = Integer.parseInt(boundX);
        }
        if (!(boundY.equals(NOT_FOUND))) {
            ry = Integer.parseInt(boundY);
        }
        r.setBound(rx, ry);
        return r;
    }

    /**
     * parse a point from to isvg.
     *
     * @param xn x attribute name
     * @param yn y attribute name
     * @param atts attributes
     *
     * @return new Point
     */
    static Point parsePoint(
            final String xn,
            final String yn,
            final Attributes atts
    ) {
        float x = parseFloat(xn, atts);
        float y = parseFloat(yn, atts);

        return new Point(
                Math.round(x),
                Math.round(y)
        );
    }

    /**
     * parse an integer from xml.
     *
     * check the case "none" and no attributes found.
     *
     * @param name name of the xml object
     * @param atts attributes of the object
     * @return new value
     */
    static int parseInt(final String name, final Attributes atts) {
        return Math.round(parseFloat(name, atts));
    }

    /**
     * parse a double number from xml.
     *
     * check the case "none" and no attributes found.
     *
     * @param name name of the xml object
     * @param atts attributes of the object
     * @return new value
     */
    static double parseDouble(final String name, final Attributes atts) {
        double result = 0.0;
        String searchedString = getAttributeValueOf(name, atts);
        if (!(searchedString.equals(NOT_FOUND))
            && !(searchedString.equals("none"))
        ) {
                result = Double.parseDouble(searchedString);
                LOGGER.debug(name + GIVES + result);
        }
        return result;
    }

    /**
     * parse a float number from xml.
     *
     * check the case "none" and no attributes found.
     *
     * @param name name of the xml object
     * @param atts attributes of the object
     * @return new value
     */
    static float parseFloat(final String name, final Attributes atts) {
        float result = 0.0f;
        String searchedString = getAttributeValueOf(name, atts);
        if (!(searchedString.equals(NOT_FOUND))
            && !(searchedString.equals("none"))
            && !(searchedString.equals(""))
        ) {
                try {
                    result = Float.parseFloat(searchedString);
                    LOGGER.debug(name + GIVES + result);
                } catch (NumberFormatException e) {
                    LOGGER.error("\"" + name + "\" is not a number !", e);
                }
            }
        return result;
    }

    /**
     * decode an hexadecimal color to a CustomColor object.
     *
     * @param colorStr hexadecimal color value
     * @return CustomColor object
     */
    static CustomColor parseColor(final String colorStr) {
        CustomColor c = null;
        if (!(colorStr.equals(NOT_FOUND))) {
            if (colorStr.contains("#")) {
                c = CustomColor.decode(colorStr);
            } else {
                c = getStandardColor(colorStr);
            }
        }
        if (c != null) {
            LOGGER.debug(colorStr + GIVES + c.toString());
        } else {
            LOGGER.debug(colorStr + " is not allowed");
        }
        return c;
    }

    /**
     * get a standard color from name.
     *
     * @param s name of the color
     * @return new CustomColor
     */
    static CustomColor getStandardColor(final String s) {
        CustomColor c = null;
        switch (s) {
            case "maroon":
                c = CustomColor.decode("#800000");
                break;
            case "red":
                c = CustomColor.red;
                break;
            case "orange":
                c = CustomColor.orange;
                break;
            case "yellow":
                c = CustomColor.yellow;
                break;
            case "olive":
                c = CustomColor.decode("#808000");
                break;
            case "purple":
                c = CustomColor.decode("#800080");
                break;
            case "fuchsia":
                c = CustomColor.MAGENTA;
                break;
            case "white":
                c = CustomColor.white;
                break;
            case "lime":
                c = CustomColor.decode("#00FF00");
                break;
            case "green":
                c = CustomColor.green;
                break;
            case "navy":
                c = CustomColor.decode("#000080");
                break;
            case "blue":
                c = CustomColor.BLUE;
                break;
            case "aqua":
                c = CustomColor.decode("#00ffff");
                break;
            case "teal":
                c = CustomColor.decode("#008080");
                break;
            case "black":
                c = CustomColor.BLACK;
                break;
            case "silver":
                c = CustomColor.decode("#c0c0c0");
                break;
            case "gray":
                c = CustomColor.gray;
                break;
            case "none":
                c = new CustomColor(true);
                break;
            default:
                break;
        }
        if (c != null) {
            LOGGER.debug(s + GIVES + c.toString());
        } else {
            LOGGER.debug(s + " is not allowed");
        }
        return c;
    }

}
