/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.model.isvgparser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.swing.JOptionPane;

/**
 * This class handle all SAX errors.
 * @since 1.0
 * @version 1.0
 * @author David Haioum
 */
public class ISvgErrorHandler implements ErrorHandler {
    /**
     * LOGGER of the class.
     */
    private static final Logger LOGGER = LogManager.getLogger(
            ISvgErrorHandler.class
    );

    /**
     * is the file valid ?
     */
    private boolean isValid = false;

    /**
     * handle error messages.
     *
     * @param e exception
     * @throws SAXException SAX exception
     */
    public void error(final SAXParseException e) throws SAXException {
        LOGGER.error(e.getMessage());
        JOptionPane.showMessageDialog(
                null,
                "ERROR : " + e.getMessage(),
                "Validating",
                JOptionPane.WARNING_MESSAGE);
        isValid = false;
        throw e;
    }
    /**
     * handle fatal error messages.
     *
     * @param e exception
     * @throws SAXException SAX exception
     */
    public void fatalError(final SAXParseException e) throws SAXException {
        LOGGER.fatal(e.getMessage());
        JOptionPane.showMessageDialog(
                null,
                "FATAL ERROR : " + e.getMessage(),
                "Validating",
                JOptionPane.WARNING_MESSAGE);
        isValid = false;
        throw e;
    }
    /**
     * handle warning messages.
     *
     * @param e exception
     * @throws SAXException SAX exception
     */
    public void warning(final SAXParseException e) throws SAXException {
        LOGGER.warn(e.getMessage());
    }

    /**
     * set the boolean isValid.
     *
     * @return true if valid
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * set the isValid attribute.
     *
     * @param valid isValid
     */
    public void setValid(final boolean valid) {
        isValid = valid;
    }
}
