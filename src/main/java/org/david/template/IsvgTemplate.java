package org.david.template;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import java.awt.event.ActionListener;

/**
 * template insert utility class for isvg files.
 */
public final class IsvgTemplate {
    private static final Logger logger = LogManager.getLogger(IsvgTemplate.class);

    /**
     * xml and svg template.
     */
    public static final String ISVG_TMPL =
            "<?xml version=\"1.0\" standalone=\"no\"?>\n"
            + "<svg viewBox=\"0 0 1200 400\" "
            + "xmlns=\"http://www.w3.org/2000/svg\" \n\t\t"
            + "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
            + "\n\n\n\n</svg>";

    /**
     * rectangle template.
     */
    public static final String RECT_TMPL = "<rect x=\"\" y=\"\" height=\"\" "
            + "width=\"\" fill=\"\" stroke=\"\" stroke-width=\"\"/>";

    /**
     * ellipse template.
     */
    public static final String ELLIPSE_TMPL = "<ellipse cx=\"\" "
            + "cy=\"\" rx=\"\" ry=\"\" fill=\"\" "
            + "stroke=\"\" stroke-width=\"\" />";
    /**
     * g transform.
     */
    public static final String TRANSFORM_TMPL = "<g transform=\"\" "
            + "stroke=\"\" stroke-width=\"\" fill=\"\" >\n\n\n\n</g>";
    /**
     * circle template.
     */
    public static final String CIRCLE_TMPL = "<circle cx=\"\" cy=\"\" r=\"\""
            + "\tfill=\"\" stroke=\"\" stroke-width=\"\"/>";
    /**
     * line template.
     */
    public static final String LINE_TMPL = "<line x1=\"\" y1=\"\" x2=\"\" "
            + "y2=\"\" stroke-width=\"\"  />";
    /**
     * polyline template.
     */
    public static final String POLYLINE_TMPL = "<polyline fill=\"\" "
            + "stroke=\"\" stroke-width=\"\" points=\"\" />";
    /**
     * polygon template.
     */
    public static final String POLYGONE_TMPL = "<polygon fill=\"\" "
            + "stroke=\"\" stroke-width=\"\" points=\"\" />";
    /**
     * text template.
     */
    public static final String TEXT_TMPL = "<text x=\"\" y=\"\" "
            + "stroke=\"\"></text>";
    /**
     * isen magic template.
     */
    public static final String ISEN_MAGIC_TMPL = "<isen_magic x=\"\" y=\"\" />";

    /**
     * private constructor (utility class).
     */
    private IsvgTemplate() {
    }

    /**
     * insert in a JTextPane a string template.
     *
     * @param container container that will contain the string.
     * @param template template string
     */
    public static void insert(final JTextPane container,
                              final String template
    ) {
        try {
            container.getDocument().insertString(
                    container.getCaretPosition(),
                    template,
                    null
            );
        } catch (BadLocationException e) {
            logger.error("bad location when "
                    + template
                    + "added at"
                    + container.getCaretPosition()
            );
        }
    }

    /**
     * create an action listener to insert a template in a container.
     *
     * @param container container that will contain the string
     * @param template string to insert
     * @return ActionListener
     */
    public static ActionListener createActionListener(
            final JTextPane container,
            final String template) {
        return e -> insert(container, template);
    }
}
