/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.vue.window;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.david.files.IsvgFilter;
import org.david.model.isvgparser.ISvgParser;
import org.david.template.IsvgTemplate;
import org.david.vue.drawingzone.DrawingZone;
import org.david.vue.isvgeditor.IsvgEditor;
import lib.textlinenumber.TextLineNumber;

import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTextPane;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.KeyStroke;
import javax.swing.JCheckBoxMenuItem;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicScrollBarUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.BorderLayout;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;

import java.util.ArrayList;

/**
 * Create the main window of the application.
 */
public class Window extends JFrame {

/* ###### CONSTANTS ###### */
    /**
     * LOGGER of the class.
     */
    private static final Logger LOGGER = LogManager.getLogger(Window.class);
    /**
     * application name.
     */
    private static final String APP_NAME = "Isen SVG";
    /**
     * dark mode hexadecimal color.
     */
    private static final String DARK_MODE_COLOR = "#4f4f4f";

    private static final String NIMBUS_DEFAULT_COLOR = "#f2f2f2";
    /**
     * default width of the window.
     */
    private static final int WIDTH = 1700;
    /**
     * default height of the window.
     */
    private static final int HEIGHT = 1000;
    /**
     * divider default size in split panel.
     */
    private static final int DEFAULT_DIVIDED_SIZE = 5;
    /**
     * default font size of text.
     */
    private static final int DEFAUT_FONT_SIZE = 18;
    /**
     * max number of digit displayed in the editor line number display.
     */
    private static final int LINE_NUMBER_MAX_DIGIT = 3;

/* ###### MODEL RELATED COMPONENTS ###### */

    /**
     * drawingZone at the left.
     */
    private DrawingZone drawingZone = null;
    /**
     * isvg editor.
     */
    private IsvgEditor isvgEditor;

/* ###### VUE RELATED COMPONENTS ###### */

    /**
     * bottom toolbar.
     */
    private JToolBar toolbar;
    /**
     * line number on the side.
     */
    private TextLineNumber lineNumber = null;
    /**
     * menu bar.
     */
    private JMenuBar menubar = null;

    /**
     * isvg official parser.
     */
    private ISvgParser parser;

    /**
     * split pane in the center.
     */
    private JSplitPane splitPane = null;
    /**
     * scrollEditor.
     */
    private JScrollPane scrollEditor = null;

/* ###### BOOLEANS ###### */

    /**
     * auto refresh Boolean.
     */
    private Boolean isAutoRefreshEnabled = false;

/* ###### TEMPORARY ATTRIBUTES ###### */

/* ###### CONSTRUCTOR ###### */

    /**
     * main constructor of the Window.
     */
    public Window() {

    /* ###### windows properties ###### */

        //call the super constructor
        super(APP_NAME);
        this.setSize(WIDTH, HEIGHT);
        //window location
        this.setLocationRelativeTo(null);
        //attach the "X" from Windows to the exit function
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    /* ###### create components ####### */

        //init isvg editor
        this.createIsvgEditor();
        //create the drawing zone in the middle
        this.createDrawingZone();
        //create the split pane in the center
        this.createSplitPane();
        //create a menu bar on the top
        this.createMenuBar();
        //create the toolbar on the bottom
        this.createToolbar();
        //set lookAndFeel
        this.setCustomLookAndFeel();
        this.parser = new ISvgParser();
    }

/* ###### UTILITIES METHODS ###### */

    /**
     * set custom look and feel for the app.
     */
    private void setCustomLookAndFeel() {
        String name = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
        try {
            UIManager.setLookAndFeel(name);
        } catch (ClassNotFoundException e) {
            LOGGER.error("look and feel \"" + name + "\" not found !");
        } catch (InstantiationException e) {
            LOGGER.error("look and feel initialization failed");
        } catch (IllegalAccessException e) {
            LOGGER.error("can't access to look and feel \"" + name + "\"");
        } catch (UnsupportedLookAndFeelException e) {
            LOGGER.error("unsupported look and feel \"" + name + "\"");
        }
    }

    /**
     * create a new Document listener
     * to refresh automatically the drawing zone.
     *
     * @return new DocumentListener
     */
    private DocumentListener createRefreshListener() {
        return new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                if (isAutoRefreshEnabled) {
                    refreshDrawingZone();
                }
            }
            @Override
            public void removeUpdate(final DocumentEvent e) {
                if (isAutoRefreshEnabled) {
                    refreshDrawingZone();
                }
            }
            @Override
            public void changedUpdate(final DocumentEvent e) {
                if (isAutoRefreshEnabled) {
                    refreshDrawingZone();
                }
            }
        };
    }

    /**
     * create an icon JButton.
     *
     * @param image path to the image from resources
     * @param a action listener
     * @return new JButton
     */
    private JButton createIconButton(
            final String image,
            final ActionListener a
    ) {
        //new icon button from resources folder
        JButton b = new JButton(
                new ImageIcon(getClass().getResource(image))
        );
        //set the action listener
        b.addActionListener(a);
        //Look and feel
        b.setOpaque(false);
        b.setContentAreaFilled(false);
        b.setBorderPainted(false);
        return b;
    }

    /**
     * create a refresh button inside the toolbar.
     *
     * @return JButton created
     */
    private JButton createRefreshButton() {
        JButton refresh = new JButton("Refresh");

        refresh.setBackground(Color.white);
        refresh.setFont(
                new Font("TimesRoman", Font.PLAIN, DEFAUT_FONT_SIZE)
        );

        refresh.addActionListener(event -> refreshDrawingZone());
        return refresh;
    }

    /**
     * parse the content of the isvgEditor and refresh the drawing zone.
     */
    private void refreshDrawingZone() {
            drawingZone.setArrayOfPrimitives(
                    parser.parseIsvg(isvgEditor.getText())
            );
            drawingZone.repaint();

    }

    /**
     * callback function to open a FileChooser.
     *
     * this file chooser support only ".isvg" format
     *
     */
    private void openFileChooser() {
        //new JFileChooser
        JFileChooser openFile = new JFileChooser();
        //JFileChooser filter : .isvg only
        openFile.setFileFilter(new IsvgFilter());
        openFile.setAcceptAllFileFilterUsed(false);

        int returnVal = openFile.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File fileChosen = openFile.getSelectedFile();
            this.setTitle(APP_NAME + " [" + fileChosen.getAbsolutePath() + "]");
            setEditorTextFromFile(isvgEditor, fileChosen);
        }
    }

    /**
     * set text of a JTextPane from file input.
     *
     * @param container JTextPane container
     * @param input input file
     */
    private void setEditorTextFromFile(
            final JTextPane container,
            final File input
    ) {
        try {
            //file reader
            FileReader fr = new FileReader(input);
            BufferedReader reader = new BufferedReader(fr);

            try {
                //read first line
                String line = reader.readLine();
                //create a string builder
                StringBuilder outputBuffer = new StringBuilder();
                //for each line of the file
                while (line != null) {
                    //append to the outputBuffer the line
                    outputBuffer.append(line);
                    //add a line return
                    outputBuffer.append("\n");
                    //read next line
                    line = reader.readLine();
                }
                //set the output to the container
                container.setText(outputBuffer.toString());
            } catch (IOException e) {
                LOGGER.error("can't read line");
            }
        } catch (IOException e) {
            LOGGER.error(input + "not found in" + input.getAbsolutePath());
        }
    }

    /**
     * open a save file dialog to save an "isvg" file.
     */
    private void openSaveFileChooserDialog() {
        JFileChooser openFile = new JFileChooser();

        openFile.setDialogTitle("Specify a file to save");
        openFile.setFileFilter(new IsvgFilter());
        openFile.setAcceptAllFileFilterUsed(false);

        int userSelection = openFile.showSaveDialog(this);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            saveFileFromTextPane(
                    isvgEditor.getText(),
                    openFile.getSelectedFile().toString(),
                    ".isvg"
            );
        }
    }

    /**
     * add content to the file named fileName.
     * add the extension to the filename.
     * open a dialog message on success.
     *
     * @param content content to write
     * @param fileName file to open
     * @param extension extension of the file
     */
    private void saveFileFromTextPane(
            final String content,
            final String fileName,
            final String extension
    ) {
        String name = fileName;
        if (!fileName.endsWith(extension)) {
            name += extension;
        }
        File fileToSave = new File(name);
        try (FileWriter out = new FileWriter(fileToSave)) {
            out.write(content);
            JOptionPane.showMessageDialog(
                    null,
                    fileToSave.getAbsoluteFile()
                            + " successfully saved !",
                    "Save file",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException e) {
            LOGGER.error("can't write in \"" + fileToSave + "\"");
        }
    }

    /**
     * create custom look and field for scroll panel.
     */
    private void setCustomScrollPaneLookAndFeel() {
        scrollEditor.setBorder(BorderFactory.createEmptyBorder());
        scrollEditor.getVerticalScrollBar().setBackground(
                Color.decode(NIMBUS_DEFAULT_COLOR)
        );
        scrollEditor.getHorizontalScrollBar().setBackground(
                Color.decode(NIMBUS_DEFAULT_COLOR)
        );
        UIManager.getLookAndFeelDefaults().put("ScrollBar.thumb",
                Color.decode(NIMBUS_DEFAULT_COLOR)
        );
        UIManager.getLookAndFeelDefaults().put("ScrollBar.thumbHighlight",
                Color.decode(NIMBUS_DEFAULT_COLOR)
        );
        scrollEditor.setVerticalScrollBarPolicy(
                javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollEditor.getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Color.decode("#d9d9d9");
            }
        });
        scrollEditor.getHorizontalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Color.decode("#d9d9d9");
            }
        });
        scrollEditor.setRowHeaderView(lineNumber);
        scrollEditor.getVerticalScrollBar().setBorder(
                BorderFactory.createEmptyBorder()
        );
    }

    /**
     * create all buttons in toolbar for templates.
     */
    private void createToolbarTemplateButtons() {
        ArrayList<JButton> tab = new ArrayList<>();
        tab.add(createIconButton("/polyline.png",
                        IsvgTemplate.createActionListener(isvgEditor,
                                IsvgTemplate.POLYLINE_TMPL)));
        tab.add(createIconButton("/rectangle.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.RECT_TMPL)));
        tab.add(createIconButton("/polygon.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.POLYGONE_TMPL)));
        tab.add(createIconButton("/ellipse.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.ELLIPSE_TMPL)));
        tab.add(createIconButton("/circle.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.CIRCLE_TMPL)));
        tab.add(createIconButton("/line.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.LINE_TMPL)));
        tab.add(createIconButton("/text.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.TEXT_TMPL)));
        tab.add(createIconButton("/isen_magic.jpg",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.ISEN_MAGIC_TMPL)));
        tab.add(createIconButton("/rotate.png",
                IsvgTemplate.createActionListener(isvgEditor,
                        IsvgTemplate.TRANSFORM_TMPL)));

        for (JButton b : tab) {
            toolbar.add(b);
        }
    }

    /**
     * change current look and feel in dark.
     */
    private void darkLookAndFeelActivated() {
        toolbar.setBackground(Color.decode(DARK_MODE_COLOR));
        menubar.setBackground(Color.decode(DARK_MODE_COLOR));
        isvgEditor.setBackground(Color.decode("#333333"));
        isvgEditor.setCaretColor(Color.white);
        splitPane.setBackground(Color.decode(DARK_MODE_COLOR));
        splitPane.setBorder(BorderFactory.createEmptyBorder());
        lineNumber.setBackground(Color.decode(DARK_MODE_COLOR));
        scrollEditor.getVerticalScrollBar().setBackground(
                Color.decode(DARK_MODE_COLOR)
        );
        scrollEditor.getHorizontalScrollBar().setBackground(
                Color.decode(DARK_MODE_COLOR)
        );
        for (int i = 0; i < menubar.getMenuCount(); i++) {
            menubar.getMenu(i).setForeground(Color.black);
        }
        menubar.setBackground(Color.decode(DARK_MODE_COLOR));
    }

    /**
     * reset look and feel by default.
     */
    private void darkLookAndFeelDisabled() {
        toolbar.setBackground(Color.decode("#eeeeee"));
        menubar.setBackground(Color.decode("#eeeeee"));
        isvgEditor.setBackground(Color.white);
        isvgEditor.setCaretColor(Color.black);
        splitPane.setBackground(Color.white);
        lineNumber.setBackground(Color.decode("#e6e6e6"));
        scrollEditor.getVerticalScrollBar().setBackground(
                Color.decode(NIMBUS_DEFAULT_COLOR)
        );
        scrollEditor.getHorizontalScrollBar().setBackground(
                Color.decode(NIMBUS_DEFAULT_COLOR)
        );
        for (int i = 0; i < menubar.getMenuCount(); i++) {
            menubar.getMenu(i).setForeground(Color.black);
        }
    }

    /**
     * open a JColorChooser and change the background color of the
     * drawing zone with this color.
     */
    private void changeDrawingZoneColorFromChooser() {
        Color bg = JColorChooser.showDialog(
                null,
                "Choose a background color",
                drawingZone.getBackground()
        );
        if (bg != null) {
            drawingZone.setBackground(bg);
        }
    }

    /**
     * create the file menu.
     *
     * @return new JMenu
     */
    private JMenu createFileMenu() {
        //FILE MENU
        JMenu fileMenu = new JMenu("File");
        //New item
        JMenuItem openItem = new JMenuItem(
                "New",
                new ImageIcon(getClass().getResource("/new.png"))
        );
        openItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O,
                KeyEvent.CTRL_DOWN_MASK
        ));
        openItem.addActionListener(e -> {
            isvgEditor.setText("");
            this.setTitle(APP_NAME);
            drawingZone.getArrayOfPrimitives().clear();
            drawingZone.repaint();
        });
        fileMenu.add(openItem);

        //Open item
        JMenuItem newItem = new JMenuItem(
                "Open",
                new ImageIcon(getClass().getResource("/open.png"))
        );
        newItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N,
                KeyEvent.CTRL_DOWN_MASK
        ));
        newItem.addActionListener(e -> openFileChooser());
        fileMenu.add(newItem);

        //Save item
        JMenuItem saveItem = new JMenuItem(
                "Save",
                new ImageIcon(getClass().getResource("/save.png"))
        );
        saveItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S,
                KeyEvent.CTRL_DOWN_MASK
        ));
        saveItem.addActionListener(e -> openSaveFileChooserDialog());
        fileMenu.add(saveItem);

        //Add a separator
        fileMenu.addSeparator();
        //Quit item
        JMenuItem quitItem = new JMenuItem(
                "Quit",
                new ImageIcon(getClass().getResource("/close.png"))
        );
        quitItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Q,
                KeyEvent.CTRL_DOWN_MASK
        ));
        quitItem.addActionListener(e -> System.exit(0));
        fileMenu.add(quitItem);

        return fileMenu;
    }

    /**
     * create the About menu.
     *
     * @return new JMenu
     */
    private JMenu createAboutMenu() {
        //ABOUT MENU
        JMenu aboutMenu = new JMenu("About");
        //[ABOUT MENU ITEMS]
        //AUTHOR ITEM
        JMenuItem authorItem = new JMenuItem(
                "Author",
                new ImageIcon(getClass().getResource("/author.png"))
        );
        authorItem.addActionListener(e -> JOptionPane.showMessageDialog(
                null,
                "David Haioum - david.haioum@isen.yncrea.fr",
                "About the Author",
                JOptionPane.INFORMATION_MESSAGE
        ));
        aboutMenu.add(authorItem);

        return aboutMenu;
    }

    /**
     * create the View menu.
     *
     * @return new JMenu
     */
    private JMenu createViewMenu() {
        //VIEW MENU
        JMenu viewMenu = new JMenu("View");
        //enable dark mode item
        JCheckBoxMenuItem darkCheck = new JCheckBoxMenuItem(
                "dark mode",
                new ImageIcon(getClass().getResource("/dark.png"))
        );
        darkCheck.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_D,
                KeyEvent.ALT_DOWN_MASK
        ));
        darkCheck.addItemListener(e -> {
            if (darkCheck.getState()) {
                darkLookAndFeelActivated();
            } else {
                darkLookAndFeelDisabled();
            }
        });
        viewMenu.add(darkCheck);
        //display toolbar border
        JCheckBoxMenuItem toolbarBorderCheck = new JCheckBoxMenuItem(
                "toolbar border",
                new ImageIcon(getClass().getResource("/toolbar.png"))
        );
        toolbarBorderCheck.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_B,
                KeyEvent.ALT_DOWN_MASK
        ));
        toolbarBorderCheck.addActionListener(e -> {
            if (toolbarBorderCheck.getState()) {
                toolbar.setBorderPainted(true);
            } else {
                toolbar.setBorderPainted(false);
            }
        });
        viewMenu.add(toolbarBorderCheck);
        //Chose background color
        JMenuItem backgroundColorPicker = new JMenuItem(
                "Background CustomColor",
                new ImageIcon(getClass().getResource("/color_picker.png"))
        );
        backgroundColorPicker.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C,
                KeyEvent.ALT_DOWN_MASK
        ));
        backgroundColorPicker.addActionListener(e ->
            changeDrawingZoneColorFromChooser()
        );
        viewMenu.add(backgroundColorPicker);

        return viewMenu;
    }

    /**
     * create the Tools menu.
     *
     * @return new JMenu
     */
    private JMenu createToolsMenu() {
        //TOOLS MENU
        JMenu toolsMenu = new JMenu("Tools");
        //auto refresh enabled item
        JCheckBoxMenuItem autoRefreshItem = new JCheckBoxMenuItem(
                "auto refresh",
                new ImageIcon(getClass().getResource("/refresh.png"))
        );
        autoRefreshItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_R,
                KeyEvent.ALT_DOWN_MASK
        ));
        autoRefreshItem.addItemListener(e ->
                isAutoRefreshEnabled = autoRefreshItem.getState()
        );
        toolsMenu.add(autoRefreshItem);
        //force refresh item
        JMenuItem processItem = new JMenuItem(
                "process",
                new ImageIcon(getClass().getResource("/play.png"))
        );
        processItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0)
        );
        processItem.addActionListener(e -> refreshDrawingZone());
        toolsMenu.add(processItem);
        //validate item
        toolsMenu.add(autoRefreshItem);
        //force refresh item
        JMenuItem validateItem = new JMenuItem(
                "validate",
                new ImageIcon(getClass().getResource("/validate.png"))
        );
        validateItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0)
        );
        validateItem.addActionListener(
                e -> LOGGER.info("validate button was pressed")
        );
        toolsMenu.add(validateItem);

        return toolsMenu;
    }

    /**
     * create the Template menu.
     *
     * @return new JMenu
     */
    private JMenu createTemplatesMenu() {
        //TEMPLATE MENU
        JMenu templateMenu = new JMenu("Template");
        //rectangle
        JMenuItem rectItem = new JMenuItem(
                "Rectangle",
                new ImageIcon(getClass().getResource("/rectangle.png"))
        );
        rectItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK)
        );
        rectItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.RECT_TMPL
        ));
        templateMenu.add(rectItem);

        //circle template item
        JMenuItem circleItem = new JMenuItem(
                "Circle",
                new ImageIcon(getClass().getResource("/circle.png"))
        );
        circleItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK)
        );
        circleItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.CIRCLE_TMPL
        ));
        templateMenu.add(circleItem);

        //line template item
        JMenuItem lineItem = new JMenuItem(
                "Line",
                new ImageIcon(getClass().getResource("/line.png"))
        );
        lineItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK)
        );
        lineItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.LINE_TMPL
        ));
        templateMenu.add(lineItem);

        //polyline template item
        JMenuItem polylineItem = new JMenuItem(
                "Polyline",
                new ImageIcon(getClass().getResource("/polyline.png"))
        );
        polylineItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_K, KeyEvent.CTRL_DOWN_MASK)
        );
        polylineItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.POLYLINE_TMPL
        ));
        templateMenu.add(polylineItem);

        //polygon template item
        JMenuItem polygonItem = new JMenuItem(
                "Polygon",
                new ImageIcon(getClass().getResource("/polygon.png"))
        );
        polygonItem.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_J, KeyEvent.CTRL_DOWN_MASK)
        );
        polygonItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.POLYGONE_TMPL
        ));
        templateMenu.add(polygonItem);

        //text template item
        JMenuItem textItem = new JMenuItem(
                "Text",
                new ImageIcon(getClass().getResource("/text.png"))
        );
        textItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_T, KeyEvent.CTRL_DOWN_MASK)
        );
        textItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.TEXT_TMPL
        ));
        templateMenu.add(textItem);

        //isen magic template item
        JMenuItem isenMagicItem = new JMenuItem(
                "Isen Magic",
                new ImageIcon(getClass().getResource("/isen_magic.jpg"))
        );
        isenMagicItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK)
        );
        isenMagicItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.ISEN_MAGIC_TMPL
        ));
        templateMenu.add(isenMagicItem);

        //ellipse template item
        JMenuItem ellipseItem = new JMenuItem(
                "Ellipse",
                new ImageIcon(getClass().getResource("/ellipse.png"))
        );
        ellipseItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK)
        );
        ellipseItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.ELLIPSE_TMPL
        ));
        templateMenu.add(ellipseItem);

        //transform template item
        JMenuItem transformItem = new JMenuItem(
                "Transform",
                new ImageIcon(getClass().getResource("/rotate.png"))
        );
        transformItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_G, KeyEvent.CTRL_DOWN_MASK)
        );
        transformItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.TRANSFORM_TMPL
        ));
        templateMenu.add(transformItem);

        //svg template item
        JMenuItem svgTemplateItem = new JMenuItem(
                "Xml",
                new ImageIcon(getClass().getResource("/svg.png"))
        );
        svgTemplateItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK)
        );
        svgTemplateItem.addActionListener(IsvgTemplate.createActionListener(
                isvgEditor,
                IsvgTemplate.ISVG_TMPL
        ));
        templateMenu.add(svgTemplateItem);

        return templateMenu;
    }

    /* ###### CREATE COMPONENTS METHODS ###### */

    /**
     * create a JMenuBar.
     *
     */
    private void createMenuBar() {
        menubar = new JMenuBar();
        menubar.setBorderPainted(false);

        //Add the menus in menu bar
        menubar.add(createFileMenu());
        menubar.add(createAboutMenu());
        menubar.add(createViewMenu());
        menubar.add(createToolsMenu());
        menubar.add(createTemplatesMenu());

        //set Font
        for (
                int menuIndex = 0;
                menuIndex < menubar.getMenuCount();
                menuIndex++
        ) {
            menubar.getMenu(menuIndex).setFont(
                    new Font("TimesRoman", Font.PLAIN, DEFAUT_FONT_SIZE)
            );
        }

        this.setJMenuBar(menubar);
    }

    /**
     * add a split panel in the center of the main window.
     */
    private void createSplitPane() {
        //new split pane split horizontally
        splitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT
        );
        //no border look and feel
        splitPane.setBorder(BorderFactory.createEmptyBorder());
        //divider in the middle
        splitPane.setDividerLocation(WIDTH / 2);
        //divider size smaller
        splitPane.setDividerSize(DEFAULT_DIVIDED_SIZE);
        splitPane.setOneTouchExpandable(true);

        //add the scrollable isvg editor on the left
        splitPane.add(scrollEditor);
        //add the drawing zone on the right
        splitPane.add(drawingZone);

        //add the split pane to the main window
        this.add(splitPane, BorderLayout.CENTER);
    }

    /**
     * create the drawing zone.
     */
    private void createDrawingZone() {
        //new drawing zone
        drawingZone = new DrawingZone();
        drawingZone.setBackground(Color.decode("#f8f8f8"));
    }

    /**
     * create the isvg editor.
     */
    private void createIsvgEditor() {
        //new isvg editor
        isvgEditor = new IsvgEditor();
        isvgEditor.setBorder(BorderFactory.createEmptyBorder());
        //add xml header template
        IsvgTemplate.insert(isvgEditor, IsvgTemplate.ISVG_TMPL);
        //listener
        isvgEditor.getDocument().addDocumentListener(createRefreshListener());
        //text line number component
        lineNumber = new TextLineNumber(isvgEditor, LINE_NUMBER_MAX_DIGIT);
        lineNumber.setBorder(BorderFactory.createEmptyBorder());
        lineNumber.setBackground(Color.decode("#e6e6e6"));
        //scrollPane
        scrollEditor = new JScrollPane(isvgEditor);
        setCustomScrollPaneLookAndFeel();
    }
    /**
     * create the toolbar in the south of the window.
     */
    private void createToolbar() {
        //new flow layout grid
        toolbar = new JToolBar();
        toolbar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolbar.setRollover(true);
        toolbar.setBorderPainted(false);
        //add the refresh button
        toolbar.add(createRefreshButton());
        toolbar.addSeparator();
        //template buttons in toolbar.
        createToolbarTemplateButtons();
        //add it in the south of the main Window
        this.add(toolbar, BorderLayout.SOUTH);
    }

}
