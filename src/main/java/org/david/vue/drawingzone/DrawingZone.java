/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.vue.drawingzone;


import org.david.geo.primitive.Primitive;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 * class to create a drawing zone to draw in a 2D graphic.
 */
public class DrawingZone extends JPanel {
    /**
     * a list of primitive to display on the graph.
     */
    private ArrayList<Primitive> arrayOfPrimitives;


    /**
     * initialize a drawing zone.
     */
    public DrawingZone() {
        super();
        arrayOfPrimitives = new ArrayList<>();
    }

    /**
     * get the array of Primitive from drawing zone.
     *
     * @return list of Primitive
     */
    public ArrayList<Primitive> getArrayOfPrimitives() {
        return arrayOfPrimitives;
    }

    /**
     * set the array of Primitive from drawing zone.
     *
     * @param primitives input array (e.g : parser return the array)
     */
    public void setArrayOfPrimitives(
            final ArrayList<Primitive> primitives
    ) {
        this.arrayOfPrimitives = primitives;
    }

    /**
     * paint components on 2D graphics.
     *
     * @param g graphic component
     */
    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);

        for (Primitive p : arrayOfPrimitives) {
            p.draw(g);
        }
    }
}
