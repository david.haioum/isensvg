/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.vue.isvgeditor;

import lib.xmleditor.XmlTextPane;

import java.awt.Font;

/**
 * This class define the isvgEditor from a XmlTextPane.
 */
public class IsvgEditor extends XmlTextPane {
    /**
     * default font size.
     */
    private static final int FONT_SIZE = 18;

    /**
     * default constructor.
     */
    public IsvgEditor() {
        super();
        this.setFont(new Font("TimesRoman", Font.PLAIN, FONT_SIZE));
    }

}
