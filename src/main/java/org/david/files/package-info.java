/**
 * This package contains all classes related to ".isvg" files.
 * Especially the filters when a user open or save a file in the software.
 */
package org.david.files;
