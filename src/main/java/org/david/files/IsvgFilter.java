/*
 * Copyright (c) 2019
 *
 * Licensed to HAIOUM DAVID under one or more contributor license agreements.
 * M.HAIOUM DAVID licenses this file to You under the Apache License,
 * Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.david.files;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * {@code IsvgFilter} is a class used by {@code JFileChooser}
 * to filter files of type ".isvg".
 *
 * @author David Haioum
 */
public class IsvgFilter extends FileFilter {
    /**
     * verify the extension isvg for the chosen file.
     *
     * @param f file to verify
     * @return true if the file is to be accepted
     */
    @Override
    public boolean accept(final File f) {
        if (f.isDirectory()) {
            return true;
        }
        String fileExtension = f.getName().substring(
                f.getName().lastIndexOf('.')
        );
        return fileExtension.equals(".isvg");
    }

    /**
     * The description of this filter.
     *
     * @return the description of this filter
     */
    @Override
    public String getDescription() {
        return ".isvg (ISEN Scalable VEctorial Graphic)";
    }
}
